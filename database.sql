drop table if exists cbrnews.new, cbrnews.user, cbrnews.audit_action, cbrnews.audit_news, cbrnews.notification_type, cbrnews.user_notification, cbrnews.session, cbrnews.new_status;

create table if not exists cbrnews.new_status(
  name VARCHAR(10) not null primary key
);
CREATE SEQUENCE if not exists user_sequence;

create table if not exists cbrnews.user(
  id INT not null primary key DEFAULT NEXTVAL ('user_sequence'),
  name varchar(50) not null,
  email varchar(50) not null unique,
  profile varchar(10) not null,
  password varchar(100) not null,
  create_date TIMESTAMP not null,
  update_date TIMESTAMP
);


CREATE SEQUENCE if not exists new_sequence;

create table if not exists cbrnews.new (
  id INT not null primary key DEFAULT NEXTVAL ('new_sequence'),
  title VARCHAR(50) not null,
  body VARCHAR(1000) not null,
  votes INT default 0 not null,
  status VARCHAR(10) default 1 not null,
  created_by INT not null,
  create_date TIMESTAMP not null,
  update_date TIMESTAMP,

  CONSTRAINT fk_new_user FOREIGN KEY (created_by) REFERENCES cbrnews.user(id),
  CONSTRAINT fk_new_status FOREIGN KEY (status) REFERENCES cbrnews.new_status(name)
);

create table if not exists cbrnews.audit_action(
  name varchar(10) not null primary key
);

CREATE SEQUENCE if not exists audit_news_sequence;

create table if not exists cbrnews.audit_news(
  id INT not null primary key default NEXTVAL('audit_news_sequence'),
  new_id INT not null,
  audit_action varchar(10) not null,
  user_id int not null,
  create_date TIMESTAMP not null,

  CONSTRAINT fk_audit_news_new FOREIGN KEY (new_id) REFERENCES cbrnews.new(id),
  CONSTRAINT fk_audit_news_action FOREIGN KEY (audit_action) REFERENCES cbrnews.audit_action(name),
  CONSTRAINT fk_audit_news_user FOREIGN KEY (user_id) REFERENCES cbrnews.user(id)
);

create table if not exists cbrnews.notification_method(
  name VARCHAR(10) not null primary key
);

CREATE SEQUENCE if not exists user_notification_sequence;

create table if not exists cbrnews.user_notification(
  id INT not null primary key default NEXTVAL ('user_notification_sequence'),
  notification_method varchar(10) not null,
  user_id INT not null,
  create_date TIMESTAMP NOT null,
  update_date TIMESTAMP not null,

  CONSTRAINT fk_user_notification_user FOREIGN KEY (user_id) REFERENCES cbrnews.user(id),
  CONSTRAINT fk_user_not_notification_type FOREIGN KEY (notification_method) REFERENCES cbrnews.notification_method(name)
);

create table if not exists cbrnews.session(
  token VARCHAR(300) not null primary key,
  user_id INT not null,
  expires_date TIMESTAMP not null,
  status varchar(10) not null default 'ALIVE',
  create_date TIMESTAMP not null,

  CONSTRAINT fk_session_user FOREIGN KEY (user_id) REFERENCES cbrnews.user(id)
);
CREATE UNIQUE INDEX unique_session ON cbrnews.session (user_id) WHERE status = 'ALIVE';

create table if not exists cbrnews.notification_event(
  name VARCHAR(10) not null primary key
);


CREATE SEQUENCE if not exists pending_notification_sequence;

create table if not exists cbrnews.news_pending_notification(
  id INT not null primary key default NEXTVAL('pending_notification_sequence'),
  new_id INT not null,
  notification_event VARCHAR(10) not null,
  create_date TIMESTAMP NOT null,
  CONSTRAINT unique_new_event unique (new_id, notification_event),
  CONSTRAINT fk_user_news_pending_notification_new FOREIGN KEY (new_id) REFERENCES cbrnews.new(id),
  CONSTRAINT fk_user_not_notification_event FOREIGN KEY (notification_event) REFERENCES cbrnews.notification_event(name)
);


---------- DATA AUX ----------
--pass123
--insert into user (name,email,profile,password,create_date) select ('admin','admin@mail.com','ADMIN','{bcrypt}$2a$10$5JbaVDtZXoWRY5ovdlE/OeM2NtBLmEoWf.QYNDcneolTNNLh1p9PW', current_timestamp) where not exists (select * from user where email='admin@mail.com');
insert into cbrnews.user (name,email,profile,password,create_date) values ('admin','admin@mail.com','ADMIN','{bcrypt}$2a$10$5JbaVDtZXoWRY5ovdlE/OeM2NtBLmEoWf.QYNDcneolTNNLh1p9PW', current_timestamp);

--notification_type
insert into cbrnews.notification_event values ('UPVOTE');
insert into cbrnews.notification_event values ('UPDATE');

-- new_status
insert into cbrnews.new_status values ('DRAFT');
insert into cbrnews.new_status values ('PUBLISHED');
insert into cbrnews.new_status values ('DELETED');

--cbrnews.notification_method
insert into cbrnews.notification_method values ('SMS');
insert into cbrnews.notification_method values ('PUSH');
insert into cbrnews.notification_method values ('EMAIL');

--cbrnews.audit_action
insert into cbrnews.audit_action values ('READ');
insert into cbrnews.audit_action values ('CREATE');
insert into cbrnews.audit_action values ('UPDATE');
insert into cbrnews.audit_action values ('DELETE');
insert into cbrnews.audit_action values ('PUBLISH');
insert into cbrnews.audit_action values ('UPVOTE');
