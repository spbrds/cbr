package pt.cbr.news.controller;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.NewsItemFilterDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.request.PaginatedRequest;
import pt.cbr.news.service.response.PaginatedResponse;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"USER"})
public class GetNewItemListController extends AbstractRestService<PaginatedRequest<NewsItemFilterDTO>, PaginatedResponse<NewsItemDTO>>{

	@Autowired
	private NewsDAO dao;
	
	@RequestMapping(path= "/new" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response getList(@RequestBody @Valid PaginatedRequest<NewsItemFilterDTO> request) {
		return super.process(request);
	}
	
	@Override
	public PaginatedResponse<NewsItemDTO> execute(PaginatedRequest<NewsItemFilterDTO> input) throws Exception {		
		input.setFilter(input.getFilter() == null ? new NewsItemFilterDTO() : input.getFilter());
		input.getFilter().setStatus(NewsItemStatus.PUBLISHED.toString());
		return dao.getList(input);	
		
	}

}
