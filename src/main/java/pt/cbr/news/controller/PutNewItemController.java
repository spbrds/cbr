package pt.cbr.news.controller;

import java.time.LocalDateTime;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.audit.AuditService;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.exceptions.UnauthorizedException;
import pt.cbr.news.service.notification.NotificationService;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"USER"})
public class PutNewItemController extends AbstractRestService<NewsItemDTO,NewsItemDTO>{

	@Autowired
	private NewsDAO dao;
	
	@Autowired
	private AuditService audit;
	
	@Autowired
	private NotificationService notificationService;

	@RequestMapping(path= "/new/{id}" , method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response putItem(@PathVariable Integer id, @Valid @RequestBody NewsItemDTO dto) {	
		dto.setId(id);
		return super.process(dto);
	}
		
	@Override	
	public NewsItemDTO execute(NewsItemDTO input) throws Exception {
		NewsItemDTO item = dao.get(input.getId()).orElseThrow(()-> new NotFoundException("Item not found"));
		
		//validating business rules
		this.validate(item);
		
		//updating the item
		item.setBody(input.getBody());
		item.setTitle(input.getTitle());
		item.setUpdateDate(LocalDateTime.now());
		item.setStatus(NewsItemStatus.DRAFT.toString());
		dao.update(item);
		
		//auditing change
		audit.update(item.getId(), getAuthenticatedUser().getId());
		
		//registering notification
		notificationService.registerItemUpdate(item);
		
		return item;				
	}
	
	private void validate(NewsItemDTO dto) throws UnauthorizedException {
		if(!dto.getCreatedBy().equals(getAuthenticatedUser().getId())) {
			throw new UnauthorizedException("Unauthorized change on news article");
		}
	}

}
