package pt.cbr.news.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.SessionDAO;
import pt.cbr.news.data.dao.UserDAO;
import pt.cbr.news.data.dto.SessionDTO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.security.JWTUserService;
import pt.cbr.news.security.JWTUtils;
import pt.cbr.news.security.LoginJwtToken;
import pt.cbr.news.security.LoginRequest;
import pt.cbr.news.security.LoginResponse;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
public class LoginController extends AbstractRestService<LoginRequest,LoginResponse>{

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private SessionDAO sessionDAO;
	
	@Autowired
	private JWTUserService jwtService;
	
	@Autowired
	private JWTUtils jwtUtils;
	
	@RequestMapping(path = "/login/oauth", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response post(@Valid @RequestBody LoginRequest loginRequest) {
		return super.process(loginRequest);
	}
	
	@Override
	public LoginResponse execute(LoginRequest login) throws Exception {
		LoginResponse loginResponse = new LoginResponse();
		
		UserDTO userDTO = userDAO.getUserByEmail(login.getUsername()).orElseThrow(() -> new NotFoundException("User not found"));
		this.validate(userDTO, login);
		
		logger.warn("Beginning Login user:" + login.getUsername());
		
		//generating token
		LoginJwtToken token = jwtUtils.generateToken(jwtService.loadUserByUsername(login.getUsername()));
		loginResponse.setExpireDate(token.getExpireDate());
		loginResponse.setToken(token.getToken());
		
		//expiring user exising sessions
		sessionDAO.invalidateUserSessions(userDTO.getId());
		
		//creating new session
		SessionDTO session = new SessionDTO();
		session.setToken(token.getToken());  
		//session.setExpireDate(LocalDateTime.parse(formatter.format(token.getExpireDate()), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		session.setExpireDate(LocalDateTime.ofInstant(Instant.now(),ZoneOffset.UTC));
		session.setUserId(userDTO.getId());
		
		sessionDAO.create(session);
		
		
		return loginResponse;
	}

	public void validate(UserDTO userDTO, LoginRequest login) throws NotFoundException{
		
		//validating user credentials
		 		
		 if(!PasswordEncoderFactories.createDelegatingPasswordEncoder().matches(login.getPassword(), userDTO.getPassword())) {
			 throw new NotFoundException("Invalid Credentials");
		 }
		
		
	}

}
