package pt.cbr.news.controller;

import java.time.LocalDateTime;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.audit.AuditService;
import pt.cbr.news.service.exceptions.BusinessException;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"USER"})
public class PostNewItemController extends AbstractRestService<NewsItemDTO,NewsItemDTO>{

	@Autowired
	private NewsDAO dao;
	
	@Autowired
	private AuditService audit;
	
	@RequestMapping(path= "/new" , method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response createItem(@RequestBody @Valid NewsItemDTO newsItem) {
		return super.process(newsItem);
	}
	
	@Override
	public NewsItemDTO execute(NewsItemDTO newsItemDTO) throws Exception {
		UserDTO userDTO = getAuthenticatedUser();
		
		newsItemDTO.setCreateDate(LocalDateTime.now());
		newsItemDTO.setCreatedBy(userDTO.getId());
		Integer newId = dao.create(newsItemDTO);
		
		NewsItemDTO createdNew = dao.get(newId).orElseThrow(() -> new BusinessException("Unable to create item"));
		
		//auditing create
		audit.create(createdNew.getId(), userDTO.getId());
		
		return createdNew;
	}

}
