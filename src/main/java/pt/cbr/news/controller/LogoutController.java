package pt.cbr.news.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.SessionDAO;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.notification.NotificationService;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"USER","ADMIN"})
public class LogoutController extends AbstractRestService<Void,Void>{
	
	@Autowired
	SessionDAO sessionDAO;
	
	@Autowired
	NotificationService notifications;
	
	@RequestMapping(path = "/signout", method = RequestMethod.GET)
	public Void logout() {
		super.process(null);
		return null;
		
	}
	
	@Override
	public Void execute(Void input) throws Exception {
		sessionDAO.invalidateUserSessions(this.getAuthenticatedUser().getId());
		return null;
	}
}