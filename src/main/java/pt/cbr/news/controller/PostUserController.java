package pt.cbr.news.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.UserDAO;
import pt.cbr.news.data.dao.UserNotificationDAO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.data.dto.UserNotificationDTO;
import pt.cbr.news.data.dto.enums.NotificationTypeEnum;
import pt.cbr.news.data.dto.enums.UserTypeEnum;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.exceptions.BusinessException;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor=Throwable.class)
public class PostUserController extends AbstractRestService<UserDTO, UserDTO>{

	@Autowired
	private UserDAO dao;
	
	@Autowired
	private UserNotificationDAO notificationDAO; 
	
	@RequestMapping(path="/user", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response post(@Valid @RequestBody UserDTO user) {
		return super.process(user);
	}
	
		
	@Override
	public UserDTO execute(UserDTO user) throws Exception {
		this.validate(user);
		
		//encrypting password
		user.setPassword(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(user.getPassword()));
		user.setRole(UserTypeEnum.USER.toString());
		Integer createdUserId = dao.create(user);
		
		
		//registering Notification types for User
		registerNotificationMethods(user, createdUserId);
		
		//writing notifications
		if(user.getNotifications() != null) {
			user.getNotifications().forEach(n -> notificationDAO.create(new UserNotificationDTO(n,createdUserId)));
		}

		UserDTO userDTO = dao.getUserByEmail(user.getEmail()).get();
		userDTO.setPassword(null);
		
		return userDTO;
	}

	private void registerNotificationMethods(UserDTO user, Integer createdUserId) {
		// TODO Auto-generated method stub
		
	}


	public void validate(UserDTO user) throws BusinessException {
		
		//if email is already existing, throw a Business exception;
		if(dao.getUserByEmail(user.getEmail()).isPresent()){
			throw new BusinessException("Email already used");
		}
		
		//validating notification Types
		
		if(user.getNotifications() == null) {
			//OK no notifications to validate
			return;
		}
		
		for(String n : user.getNotifications()) {
			try {
				NotificationTypeEnum.valueOf(n);
			}catch(IllegalArgumentException e) {
				throw new BusinessException(String.format("Illegal notification type '%s",n));
			}
		}
		
	}
	
}
