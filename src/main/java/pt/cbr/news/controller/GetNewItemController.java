package pt.cbr.news.controller;


import javax.annotation.security.RolesAllowed;

import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.data.dto.enums.UserTypeEnum;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.audit.AuditService;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.exceptions.UnauthorizedException;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"ADMIN","USER"})
public class GetNewItemController extends AbstractRestService<Integer,NewsItemDTO>{
	
	@Autowired
	private NewsDAO dao;
	
	@Autowired
	private AuditService audit;
	
	@RequestMapping(path= "/new/{id}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response getItem(@PathVariable Integer id) {	
		//executing service
		return super.process(id);
	}

	@Override
	public NewsItemDTO execute(Integer input) throws TooManyRowsException, NotFoundException, DataAccessException, UnauthorizedException {
		
		NewsItemDTO item = dao.get(input).orElseThrow(()-> new NotFoundException("Item not found"));
		
		//validating access to the news item
		this.validate(item);
		
		//auditing read;
		audit.read(item.getId(), getAuthenticatedUser().getId());
		
		return item;
	}
	
	//validate newStatus and creator;
	private void validate(NewsItemDTO newsItem) throws NotFoundException, UnauthorizedException {
		UserDTO authUser = getAuthenticatedUser();
		
		//checking if the user is ADMIN
		if(UserTypeEnum.ADMIN.toString().equals(authUser.getRole())) {
			//access granted
			return;
		}
		
		//check if it is not deleted
		if(NewsItemStatus.DELETED.toString().equals(newsItem.getStatus())) {
			throw new NotFoundException("News article not found");
		}
		
		//checking if the user authenticated is the creator
		if(NewsItemStatus.DRAFT.toString().equals(newsItem.getStatus()) && newsItem.getCreatedBy() != authUser.getId()) {
			logger.info(String.format("Access to news item %s to user %s", newsItem.getId(), authUser.getId()) );
			throw new NotFoundException("News article not found");
		}
	}

	
}
