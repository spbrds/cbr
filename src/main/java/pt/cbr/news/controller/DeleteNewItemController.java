package pt.cbr.news.controller;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;
import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"ADMIN"})
public class DeleteNewItemController extends AbstractRestService<Integer,Void>{
	
	@Autowired
	private NewsDAO dao;
	
	@RequestMapping(path= "/new/{id}" , method = RequestMethod.DELETE)	
	public Response deleteItem(@PathVariable @NonNull Integer id) {
		return super.process(id);
	}

	@Override
	public Void execute(Integer input) throws Exception {
		dao.delete(input);
		return null;
	}

}
