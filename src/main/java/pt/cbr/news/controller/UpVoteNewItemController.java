package pt.cbr.news.controller;

import java.time.LocalDateTime;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;
import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.audit.AuditService;
import pt.cbr.news.service.exceptions.BusinessException;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.notification.NotificationService;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
@RolesAllowed({"USER"})
public class UpVoteNewItemController extends AbstractRestService<Integer, NewsItemDTO>{
	
	
	@Value("${notification.upvote.treshhold}")
	//it was working before
	private static Integer upvoteTreshhold = 10;
	
	@Autowired
	private NewsDAO newsDAO;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private AuditService auditService;
	
	@RequestMapping(path= "/new/{id}/upvote" , method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response upVote(@PathVariable @NonNull Integer id) {	
		//executing service
		return super.process(id);
	}

	@Override
	public NewsItemDTO execute(Integer id) throws Exception {
		NewsItemDTO dto = newsDAO.get(id).orElseThrow(() -> new NotFoundException("News Article not found"));
		
		this.validate(dto);
		
		dto.setVotes(dto.getVotes() + 1);
		dto.setUpdateDate(LocalDateTime.now());
		newsDAO.update(dto);
		
		//auditing the upvote
		auditService.upVote(dto.getId(), getAuthenticatedUser().getId());
		
		//registering notification
		if(dto.getVotes() == upvoteTreshhold) {
			notificationService.registerItemUpVoteTreshold(dto);
		}
			
		return dto;
	}
	
	private void validate(NewsItemDTO dto) throws BusinessException {
		if(!NewsItemStatus.PUBLISHED.toString().equals(dto.getStatus())) {
			throw new BusinessException("Illegal Operation");
		}
	}
	
	
}
