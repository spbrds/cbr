package pt.cbr.news.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.cbr.news.data.dao.NewsDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.service.AbstractRestService;
import pt.cbr.news.service.audit.AuditService;
import pt.cbr.news.service.exceptions.BusinessException;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.exceptions.UnauthorizedException;
import pt.cbr.news.service.response.Response;

@RestController
@Transactional(rollbackFor = Throwable.class)
public class PublishNewItemController extends AbstractRestService<Integer,NewsItemDTO>{
	
	@Autowired
	NewsDAO newsDAO;
	
	@Autowired
	AuditService audit;
	
	@RequestMapping(path="new/{id}/publish", method= RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Response publishItem(@PathVariable Integer id) {
		//executing service
		return super.process(id);
	}
	
	@Override
	public NewsItemDTO execute(Integer input) throws Exception {
		NewsItemDTO newsItem = newsDAO.get(input).orElseThrow(() -> new NotFoundException("News Article not found"));
		
		validate(newsItem);
		
		//updating newItem Status
		newsItem.setStatus(NewsItemStatus.PUBLISHED.toString());
		newsItem.setUpdateDate(LocalDateTime.now());
		newsDAO.update(newsItem);
		
		//auditing
		audit.publish(newsItem.getId(), getAuthenticatedUser().getId());
		
		return newsItem;
	}

	private void validate(NewsItemDTO newsItem) throws BusinessException {
		//checking if the author is the current user
		if(getAuthenticatedUser().getId()!=newsItem.getCreatedBy()) {
			throw new UnauthorizedException("User not allowed");
		}
		
		//checking status, only draft allowed
		if(!NewsItemStatus.DRAFT.toString().equals(newsItem.getStatus())){
			throw new BusinessException("Unauthorized transition");
		}
		
	}
	
	

}
