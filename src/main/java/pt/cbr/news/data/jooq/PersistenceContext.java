package pt.cbr.news.data.jooq;

import javax.sql.DataSource;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan({"pt.cbr.news.generated.data.jooq.public_.tables.New"})
@EnableTransactionManagement
@PropertySource("classpath:config.properties")
public class PersistenceContext {
	
	@Autowired
	private Environment environment;

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(environment.getRequiredProperty("db.url"));
	    dataSource.setUsername(environment.getRequiredProperty("db.username"));
	    dataSource.setPassword(environment.getRequiredProperty("db.password"));
	    
	    return dataSource;
	}
	
	@Bean
	public DefaultConfiguration getConfiguration() {
	    DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
	    jooqConfiguration.set(getConnectionProvider());
	    jooqConfiguration.set(new DefaultExecuteListenerProvider(getExceptionTransformer()));
	    jooqConfiguration.setSQLDialect(SQLDialect.POSTGRES);
	    return jooqConfiguration;
	}

	@Bean
	public TransactionAwareDataSourceProxy getTransactionAwareDataSource() {
	    return new TransactionAwareDataSourceProxy(getDataSource());
	}

	@Bean
	public DataSourceTransactionManager getTransactionManager() {
	    return new DataSourceTransactionManager(getDataSource());
	}

	@Bean
	public DataSourceConnectionProvider getConnectionProvider() {
	    return new DataSourceConnectionProvider(getTransactionAwareDataSource());
	}

	@Bean
	public JooqExceptionTranslator getExceptionTransformer() {
	    return new JooqExceptionTranslator();
	}
	    
	@Bean
	public DefaultDSLContext dsl() {
	    return new DefaultDSLContext(getConfiguration());
	}
	
}
