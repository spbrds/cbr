package pt.cbr.news.data.jooq;

import org.jooq.ExecuteContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultExecuteListener;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.jdbc.support.SQLExceptionTranslator;

public class JooqExceptionTranslator extends DefaultExecuteListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void exception(ExecuteContext context) {
		SQLDialect dialect = context.configuration().dialect();
		SQLExceptionTranslator translator = new SQLErrorCodeSQLExceptionTranslator(dialect.name());
		context.exception(translator.translate("JOOQ Database Access", context.sql(), context.sqlException()));
	}

}
