package pt.cbr.news.data.dto.enums;

public enum SessionStatusEnum {
	ALIVE,
	EXPIRED
}
