package pt.cbr.news.data.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Setter @Getter
public class NewsAuditDTO {
	
	private Integer id;
	
	@NonNull
	private Integer newsItemId;
	
	@NonNull
	private Integer userId;
	
	@NonNull
	private String auditAction;
	@NonNull
	private LocalDateTime createDate;
	
	

}
