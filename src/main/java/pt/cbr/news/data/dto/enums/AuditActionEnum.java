package pt.cbr.news.data.dto.enums;

public enum AuditActionEnum {
	CREATE,
	READ,
	UPDATE,
	DELETE, 
	PUBLISH,
	UPVOTE
}
