package pt.cbr.news.data.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter @Setter
public class UserNotificationDTO extends BusinessDTO{
	
	public UserNotificationDTO(@NonNull String notificationType, @NonNull Integer userId) {
		this.notificationType = notificationType;
		this.userId = userId;
	}
	@NonNull
	private String notificationType;
	@NonNull
	private Integer userId;

}
