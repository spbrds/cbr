package pt.cbr.news.data.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Setter @Getter
public class NewsNotificationDTO extends BusinessDTO{

	public NewsNotificationDTO(@NonNull Integer newId, @NonNull String notificationEvent) {
		super();
		this.newId = newId;
		this.notificationEvent = notificationEvent;
	}

	@NonNull
	private Integer newId;
	
	@NonNull
	private String notificationEvent;
	
	
	
	
}
