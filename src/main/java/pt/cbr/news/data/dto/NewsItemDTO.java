package pt.cbr.news.data.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class NewsItemDTO extends BusinessDTO {
	
	@Size(message ="Wrong Title Length", min = 5, max = 50)
	@NotBlank(message="Title is mandatory")
	private String title;
	
	@Size(message ="Wrong Body Length", min = 5, max = 1000)
	@NotBlank(message="Body is mandatory")
	private String body;
	private Integer createdBy;
	private Integer votes;
	private String status;
	private String createdByName;
	

}
