package pt.cbr.news.data.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class UserDTO extends BusinessDTO{

	@Size(message = "Name length constraint", max = 50, min = 3)
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	private String role;
	
	@Email
	@NotBlank(message="email is mandatory")
	private String email;
	
	private String password;
	
	private List<String> notifications;	
	
}
