package pt.cbr.news.data.dto.enums;

public enum NewsNotificationEvent {
	UPDATE,
	UPVOTE
}
