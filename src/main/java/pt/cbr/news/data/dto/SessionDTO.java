package pt.cbr.news.data.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class SessionDTO {

	private String token;
	private Integer userId;
	private LocalDateTime expireDate;
	private LocalDateTime createDate;
	private String status;
	
	
}
