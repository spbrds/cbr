package pt.cbr.news.data.dto.enums;

public enum NotificationStatusEnum {
	PROCESSED,
	PENDING
}
