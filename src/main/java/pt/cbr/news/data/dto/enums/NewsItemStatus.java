package pt.cbr.news.data.dto.enums;

public enum NewsItemStatus {
	DRAFT,
	PUBLISHED,
	DELETED
}
