package pt.cbr.news.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NewsItemFilterDTO {
	private String title;
	private String body;
	private String status;
	private Integer createdBy;
}
