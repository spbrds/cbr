package pt.cbr.news.data.dto.enums;

public enum UserTypeEnum {
	ADMIN,
	USER
}
