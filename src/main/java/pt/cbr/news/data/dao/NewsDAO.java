package pt.cbr.news.data.dao;

import static pt.cbr.news.generated.data.jooq.cbrnews.tables.New.NEW;
import static pt.cbr.news.generated.data.jooq.cbrnews.tables.User.USER;

import java.time.LocalDateTime;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.jooq.Condition;
import org.jooq.Record;
import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.NewsItemFilterDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.New;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.request.PaginatedRequest;
import pt.cbr.news.service.response.PaginatedResponse;

@Component
public class NewsDAO extends AbstractCrudDAO<NewsItemDTO, New> {


	@Override
	public Integer create(@NonNull NewsItemDTO element) {
		return getDSLContext().insertInto(NEW)
			.set(NEW.TITLE, element.getTitle())
			.set(NEW.BODY, element.getBody())
			.set(NEW.VOTES, 0)
			.set(NEW.STATUS, NewsItemStatus.DRAFT.toString())
			.set(NEW.CREATE_DATE, LocalDateTime.now())
			.set(NEW.CREATED_BY, element.getCreatedBy())
			.returning()
			.fetchOne()
			.getId();
		
			
	}

	@Override
	public Integer update(@NonNull NewsItemDTO element) {
		return getDSLContext().update(NEW)
			.set(NEW.TITLE, element.getTitle())
			.set(NEW.BODY, element.getBody())
			.set(NEW.VOTES, element.getVotes())
			.set(NEW.STATUS, element.getStatus())
			.set(NEW.CREATE_DATE, LocalDateTime.now())
			.set(NEW.CREATED_BY, element.getCreatedBy())
			.set(NEW.UPDATE_DATE, LocalDateTime.now())
			.where(NEW.ID.eq(element.getId()))
			.execute();
	}

	@Override
	public Optional<NewsItemDTO> get(@NonNull Integer id) throws TooManyRowsException, NotFoundException, DataAccessException {
		return getDSLContext().select(
				NEW.ID,
				NEW.TITLE,
				NEW.BODY,
				NEW.CREATE_DATE,
				NEW.UPDATE_DATE,
				NEW.VOTES,
				NEW.STATUS,
				NEW.CREATED_BY,
				USER.NAME
				).from(NEW)
				.join(USER).on(NEW.CREATED_BY.eq(USER.ID))
				.where(NEW.ID.eq(id))
		.fetchOptional(r-> {
			NewsItemDTO dto = mapBaseRecord(r);
			dto.setCreatedByName(r.get(USER.NAME));
			return dto;
		});	
	}

	@Override
	public void delete(@NonNull Integer id) {
		getDSLContext().update(NEW)
				.set(NEW.STATUS, NewsItemStatus.DELETED.toString())
				.set(NEW.UPDATE_DATE, LocalDateTime.now())
				.where(NEW.ID.eq(id))
				.execute();
		
	}

	public PaginatedResponse<NewsItemDTO> getList(PaginatedRequest<NewsItemFilterDTO> request) {
		PaginatedResponse<NewsItemDTO> response = new PaginatedResponse<>();
		
		Condition filter = buildListFilter(request.getFilter());
		//getting data
		response.setData(this.getListSelect(request,filter, NEW.VOTES.desc())
			.fetch().map(r -> mapBaseRecord(r)));
		
		//finishing response
		response.setPageSize(request.getPageSize());
		response.setPageNumber(request.getPageNumber());
		response.setTotalRecords(getDSLContext().selectCount().from(NEW).where(filter).fetchOne(0,Integer.class));
		
		return response;
	}
	
	private NewsItemDTO mapBaseRecord(Record r){ 
			NewsItemDTO dto = new NewsItemDTO();
				dto.setId(r.get(NEW.ID)); 
				dto.setTitle(r.get(NEW.TITLE)); 
				dto.setBody(r.get(NEW.BODY)); 
				dto.setCreatedBy(r.get(NEW.CREATED_BY)); 
				dto.setVotes(r.get(NEW.VOTES)); 
				dto.setStatus(r.get(NEW.STATUS)); 
				dto.setCreateDate(r.get(NEW.CREATE_DATE));  
				dto.setUpdateDate(r.get(NEW.UPDATE_DATE));
		return dto;
	}
	
	private Condition buildListFilter(NewsItemFilterDTO filter) {
		Condition c = DSL.trueCondition();
		
		if(filter == null) {
			return c;
		}
		
		if(!StringUtils.isEmpty(filter.getBody())) {
			c = c.and(NEW.BODY.like("%"+filter.getBody()+"%"));
		}
		if(!StringUtils.isEmpty(filter.getTitle())) {
			c = c.and(NEW.TITLE.like("%"+filter.getTitle()+"%"));
		}
		if(!StringUtils.isEmpty(filter.getStatus())) {
			c = c.and(NEW.STATUS.eq(filter.getStatus()));
		}
		if(filter.getCreatedBy() != null ) {
			c = c.and(NEW.CREATED_BY.eq(filter.getCreatedBy()));
		}
		
		return c;
	}
	
	@Override
	protected New getTable() {
		return NEW;
	}

}
