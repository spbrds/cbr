package pt.cbr.news.data.dao;




import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.OrderField;
import org.jooq.Record;
import org.jooq.SelectForUpdateStep;
import org.jooq.SelectSeekStepN;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.springframework.beans.factory.annotation.Autowired;

import pt.cbr.news.service.request.PaginatedRequest;

public abstract class AbstractCrudDAO<E, T extends TableImpl<?>> implements CrudDAO<E>{
	
	protected abstract T getTable();
	
	@Autowired
	private DSLContext dslContext;
	
	protected DSLContext getDSLContext() {
		return this.dslContext;
	}
	
	protected SelectForUpdateStep<Record> getListSelect(PaginatedRequest<?> request ,Condition condition, OrderField<?>... orderFields) { 
		SelectSeekStepN<Record> select = dslContext.select()
			.from(getTable())
			.where(condition)
			.orderBy(orderFields);
		 
		 if(request.getPageSize() > 0) {
			return select
					.limit(request.getPageSize() > 0 ? request.getPageSize() : 0)
					.offset(request.getPageNumber() * request.getPageSize());
		 }else {
			 return select;
		 }
	}
	
	protected SelectForUpdateStep<Record> getListSelect(PaginatedRequest<?> request , OrderField<?>... orderFields) {
		return this.getListSelect(request, DSL.trueCondition(), orderFields);
	}
}
