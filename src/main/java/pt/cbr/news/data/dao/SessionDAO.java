package pt.cbr.news.data.dao;

import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.NonNull;

import static pt.cbr.news.generated.data.jooq.cbrnews.tables.Session.SESSION;

import java.time.LocalDateTime;

import pt.cbr.news.data.dto.SessionDTO;
import pt.cbr.news.data.dto.enums.SessionStatusEnum;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.exceptions.UnauthorizedException;

@Component
public class SessionDAO{

	@Autowired
	private DSLContext dslContext;
	
	
	public Integer create(@NonNull SessionDTO element) {
		return dslContext.insertInto(SESSION)
		.set(SESSION.TOKEN, element.getToken())
		.set(SESSION.USER_ID, element.getUserId())
		.set(SESSION.STATUS, SessionStatusEnum.ALIVE.toString())
		.set(SESSION.EXPIRES_DATE, element.getExpireDate())
		.set(SESSION.CREATE_DATE, LocalDateTime.now())
		.execute();
		
	}

	
	public Integer update(SessionDTO element) {
		// TODO Auto-generated method stub
		return null;
	}


	public SessionDTO get(@NonNull String token) throws TooManyRowsException, NotFoundException, DataAccessException, UnauthorizedException {
		return dslContext.select(SESSION.TOKEN,SESSION.USER_ID,SESSION.STATUS,SESSION.EXPIRES_DATE,SESSION.CREATE_DATE)
				.from(SESSION)
				.where(SESSION.TOKEN.eq(token))
				.fetchOptional(
						r -> {
							SessionDTO dto = new SessionDTO();
							
							dto.setCreateDate(r.get(SESSION.CREATE_DATE));
							dto.setExpireDate(r.get(SESSION.EXPIRES_DATE));
							dto.setToken(r.get(SESSION.TOKEN));
							dto.setUserId(r.get(SESSION.USER_ID));
							dto.setStatus(r.get(SESSION.STATUS));
							
							return dto;
						}).orElseThrow(()->new UnauthorizedException("Unauthorized"));
	}
	
	public void invalidateUserSessions(@NonNull Integer userId) {
		dslContext.update(SESSION)
		.set(SESSION.STATUS, SessionStatusEnum.EXPIRED.toString())
		.where(SESSION.USER_ID.eq(userId))
		.execute();
	}

}
