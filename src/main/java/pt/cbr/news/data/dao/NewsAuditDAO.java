package pt.cbr.news.data.dao;

import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import pt.cbr.news.data.dto.NewsAuditDTO;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditNews;
import pt.cbr.news.service.exceptions.NotFoundException;

import static pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditNews.AUDIT_NEWS;

import java.util.Optional;

@Component
public class NewsAuditDAO extends AbstractCrudDAO<NewsAuditDTO, AuditNews>{

	
	@Override
	public Integer create(@NonNull NewsAuditDTO element) {
		System.out.println(String.format("new: %d, user: %d",element.getNewsItemId(),element.getUserId()));
		
		return getDSLContext().insertInto(AUDIT_NEWS)
		.set(AUDIT_NEWS.USER_ID,element.getUserId())
		.set(AUDIT_NEWS.NEW_ID,element.getNewsItemId())
		.set(AUDIT_NEWS.CREATE_DATE,element.getCreateDate())
		.set(AUDIT_NEWS.AUDIT_ACTION,element.getAuditAction())
		.returning()
		.fetchOne()
		.getId();
		
	}

	@Override
	public Integer update(@NonNull NewsAuditDTO element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<NewsAuditDTO> get(@NonNull Integer id) throws TooManyRowsException, NotFoundException, DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(@NonNull Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected AuditNews getTable() {
		return AUDIT_NEWS;
	}

}
