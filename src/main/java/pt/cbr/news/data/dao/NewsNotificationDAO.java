package pt.cbr.news.data.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import pt.cbr.news.data.dto.NewsNotificationDTO;
import pt.cbr.news.data.dto.enums.NewsItemStatus;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.service.notification.dto.NewsItemNotificationDTO;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.NewsPendingNotification;

import static pt.cbr.news.generated.data.jooq.cbrnews.tables.NewsPendingNotification.NEWS_PENDING_NOTIFICATION;
import static pt.cbr.news.generated.data.jooq.cbrnews.tables.New.NEW;

@Component
public class NewsNotificationDAO extends AbstractCrudDAO<NewsNotificationDTO, NewsPendingNotification> {

	@Override
	public Integer create(@NonNull NewsNotificationDTO element) {
		 return this.getDSLContext().insertInto(NEWS_PENDING_NOTIFICATION)
				.set(NEWS_PENDING_NOTIFICATION.NEW_ID, element.getNewId())
				.set(NEWS_PENDING_NOTIFICATION.NOTIFICATION_EVENT,element.getNotificationEvent())
				.set(NEWS_PENDING_NOTIFICATION.CREATE_DATE, LocalDateTime.now())
				.returning()
				.fetchOne()
				.getId();
				
	}

	@Override
	public Integer update(@NonNull NewsNotificationDTO element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<NewsNotificationDTO> get(@NonNull Integer id)
			throws TooManyRowsException, NotFoundException, DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(@NonNull Integer id) {
		getDSLContext()
			.delete(NEWS_PENDING_NOTIFICATION)
			.where(NEWS_PENDING_NOTIFICATION.ID.eq(id))
			.execute();				
	}
	
	public Integer isExistingNotification(NewsNotificationDTO element){
		return getDSLContext()
				.fetchCount(NEWS_PENDING_NOTIFICATION, 
						NEWS_PENDING_NOTIFICATION.NEW_ID.eq(element.getNewId()).and(NEWS_PENDING_NOTIFICATION.NOTIFICATION_EVENT.eq(element.getNotificationEvent())));				
				
	}
	
	public List<NewsItemNotificationDTO> getAllNotifications(){
		return getDSLContext().select(
				NEWS_PENDING_NOTIFICATION.ID,
				NEWS_PENDING_NOTIFICATION.NOTIFICATION_EVENT,
				NEW.ID,
				NEW.TITLE,
				NEW.BODY)
			.from(NEWS_PENDING_NOTIFICATION)
			.join(NEW).on(NEW.ID.eq(NEWS_PENDING_NOTIFICATION.NEW_ID).and(NEW.STATUS.eq(NewsItemStatus.PUBLISHED.toString())))
			.fetch().map(r -> {
				NewsItemNotificationDTO result = new NewsItemNotificationDTO(r.get(NEW.ID),
													r.get(NEWS_PENDING_NOTIFICATION.NOTIFICATION_EVENT),
													r.get(NEW.TITLE),
													r.get(NEW.BODY));
				result.setId(r.get(NEWS_PENDING_NOTIFICATION.ID));
				return result;
			});
	}
	
	@Override
	protected NewsPendingNotification getTable() {
		return NEWS_PENDING_NOTIFICATION;
	}

}
