package pt.cbr.news.data.dao;

import java.util.Optional;

import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;

import lombok.NonNull;
import pt.cbr.news.service.exceptions.NotFoundException;

public interface CrudDAO<E> {

	public Integer create(@NonNull E element);
	
	public Integer update(@NonNull E element);
	
	public Optional<E> get(@NonNull Integer id) throws TooManyRowsException, NotFoundException, DataAccessException;
	
	public void delete(@NonNull Integer id);
	
}
