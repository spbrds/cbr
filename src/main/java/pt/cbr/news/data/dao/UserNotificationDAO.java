package pt.cbr.news.data.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.jooq.Record;
import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import pt.cbr.news.data.dto.UserNotificationDTO;
import pt.cbr.news.data.dto.enums.AuditActionEnum;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.UserNotification;
import static pt.cbr.news.generated.data.jooq.cbrnews.tables.UserNotification.USER_NOTIFICATION;
import static pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditNews.AUDIT_NEWS;

import pt.cbr.news.service.exceptions.NotFoundException;

@Component
public class UserNotificationDAO extends AbstractCrudDAO<UserNotificationDTO,UserNotification>{

	@Override
	public Integer create(@NonNull UserNotificationDTO element) {
		return getDSLContext().insertInto(USER_NOTIFICATION)
		.set(USER_NOTIFICATION.NOTIFICATION_METHOD, element.getNotificationType())
		.set(USER_NOTIFICATION.USER_ID, element.getUserId())
		.set(USER_NOTIFICATION.CREATE_DATE, LocalDateTime.now())
		.set(USER_NOTIFICATION.UPDATE_DATE, LocalDateTime.now())
		.returning()
		.fetchOne()
		.getId();
		
	}

	@Override
	public Integer update(@NonNull UserNotificationDTO element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<UserNotificationDTO> get(@NonNull Integer id)
			throws TooManyRowsException, NotFoundException, DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(@NonNull Integer id) {
	
		
	}

	@Override
	protected UserNotification getTable() {
		// TODO Auto-generated method stub
		return USER_NOTIFICATION;
	}
	
	public List<UserNotificationDTO> getAllNotifiableUsers() {
		return getDSLContext().select(
				USER_NOTIFICATION.ID,
				USER_NOTIFICATION.USER_ID,
				USER_NOTIFICATION.NOTIFICATION_METHOD
				).from(USER_NOTIFICATION)
		.fetch().map(r -> {return getNotificationRecord(r);});
	}
	
	/*Select * from cbrnews.user_notification where user_id in (
	Select distinct(user_id) from cbrnews.audit_news where audit_action='READ' and new_id=9);*/
	
	public List<UserNotificationDTO> getReadUsersToNotify(@NonNull Integer newId){
		 return getDSLContext().select(
				USER_NOTIFICATION.ID,
				USER_NOTIFICATION.USER_ID,
				USER_NOTIFICATION.NOTIFICATION_METHOD
				).from(USER_NOTIFICATION)
				.where(USER_NOTIFICATION.USER_ID.in(
						getDSLContext().selectDistinct(AUDIT_NEWS.USER_ID)
						.from(AUDIT_NEWS)
						.where(AUDIT_NEWS.AUDIT_ACTION.eq(AuditActionEnum.READ.toString()).and(AUDIT_NEWS.NEW_ID.eq(newId)))
				)).fetch().map(r -> {return getNotificationRecord(r);});
	}

	
	private UserNotificationDTO getNotificationRecord(Record r) {
		UserNotificationDTO dto  = new UserNotificationDTO(r.get(USER_NOTIFICATION.NOTIFICATION_METHOD), r.get(USER_NOTIFICATION.USER_ID));
		dto.setId(r.get(USER_NOTIFICATION.ID));
		return dto;
	}
}
