package pt.cbr.news.data.dao;

import org.jboss.logging.Logger;
import org.jooq.exception.DataAccessException;
import org.jooq.exception.TooManyRowsException;
import org.springframework.stereotype.Component;

import lombok.NonNull;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.exceptions.NotFoundException;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.User;

import static pt.cbr.news.generated.data.jooq.cbrnews.tables.User.USER;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class UserDAO extends AbstractCrudDAO<UserDTO,User>{

	@Override
	public Integer create(@NonNull UserDTO element) {
		return this.getDSLContext().insertInto(USER)
		.set(USER.NAME, element.getName())
		.set(USER.PROFILE, element.getRole())
		.set(USER.EMAIL, element.getEmail())
		.set(USER.PASSWORD, element.getPassword())
		.set(USER.CREATE_DATE, LocalDateTime.now())
		.returning()
		.fetchOne()
		.getId();
	}

	@Override
	public Integer update(@NonNull UserDTO element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<UserDTO> get(Integer id) throws TooManyRowsException, NotFoundException, DataAccessException {
		Logger.getLogger(getClass()).warn("Reading on DB user -> "+id);
		return getDSLContext().select(
				USER.ID,
				USER.NAME,
				USER.EMAIL,
				USER.PROFILE,
				USER.PASSWORD,
				USER.CREATE_DATE,
				USER.UPDATE_DATE)
			.from(USER)
			.where(USER.ID.eq(id))
			.fetchOptional(
					r -> {
						UserDTO u = new UserDTO();
						
						u.setId(r.get(USER.ID));
						u.setName(r.get(USER.NAME));
						u.setEmail(r.get(USER.EMAIL));
						u.setRole(r.get(USER.PROFILE));
						u.setPassword(r.get(USER.PASSWORD));
						u.setCreateDate(r.get(USER.CREATE_DATE));
						
						return u;
					});
		
	}

	@Override
	public void delete(@NonNull Integer id) {
		// TODO Auto-generated method stub
		
	}
	
	public Optional<UserDTO> getUserByEmail(@NonNull String email) {
		return getDSLContext().select(USER.ID,USER.NAME,USER.PROFILE,USER.EMAIL,USER.PASSWORD,USER.CREATE_DATE)
		.from(USER)
		.where(USER.EMAIL.eq(email))
		.fetchOptional(
				r -> {
					UserDTO u = new UserDTO();
					u.setId(r.get(USER.ID));
					u.setName(r.get(USER.NAME));
					u.setRole(r.get(USER.PROFILE));
					u.setEmail(r.get(USER.EMAIL));
					u.setPassword(r.get(USER.PASSWORD));
					u.setCreateDate(r.get(USER.CREATE_DATE));
				
					return u;
				});	
	}

	@Override
	protected User getTable() {
		return USER;
	}
	
}
