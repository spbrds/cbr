package pt.cbr.news.security;

import javax.validation.constraints.Email;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter @Setter
public class LoginRequest {

	@NonNull
	@Email
	private String username;
	@NonNull
	private String password;
		
	
}
