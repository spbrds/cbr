package pt.cbr.news.security;

import java.util.Date;

public class LoginJwtToken {
	private String token;
	private Date expireDate;
	
	public LoginJwtToken(String token, Date expireDate) {
		this.token = token;
		this.expireDate = expireDate;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	
	

}
