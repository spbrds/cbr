package pt.cbr.news.security;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pt.cbr.news.data.dao.UserDAO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.user.UserCache;

@Service
public class JWTUserService implements UserDetailsService {

	Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private UserCache userCache;
	
		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			
			UserDTO user = userDAO.getUserByEmail(username).get();
			
			//helping cache
			logger.info(String.format("Loading and caching user %d", user.getId()));
			userCache.putUser(user);
			
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_"+user.getRole()));
			User userAuth = new User(user.getEmail(),user.getPassword(),authorities);
			
			return userAuth;	
			
	}

	
}
