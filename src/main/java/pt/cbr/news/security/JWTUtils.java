package pt.cbr.news.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.jboss.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pt.cbr.news.data.dao.SessionDAO;
import pt.cbr.news.data.dto.SessionDTO;
import pt.cbr.news.data.dto.enums.SessionStatusEnum;


@Component
public class JWTUtils implements Serializable{

	/**
	 * 
	 */
	
	Logger logger = Logger.getLogger(getClass());
	
	private static final long serialVersionUID = 1L;

	public static final int TOKEN_VALIDITY = 60 * 60 * 3;
	
	@Autowired
	private SessionDAO sessionDAO;
	
	@Value("${jwt.secret}")
	//it was working before...
	private static String secret = "a_very_secret_key";
	
	
	//retrieve username from jwt token
		public String getUsernameFromToken(String token) {
			return getClaimFromToken(token, Claims::getSubject);
		}

		//retrieve expiration date from jwt token
		public Date getExpirationDateFromToken(String token) {
			return getClaimFromToken(token, Claims::getExpiration);
		}

		public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
			final Claims claims = getAllClaimsFromToken(token);
			return claimsResolver.apply(claims);
		}
	    //for retrieveing any information from token we will need the secret key
		private Claims getAllClaimsFromToken(String token) {
			return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		}

		//check if the token has expired
		private boolean isTokenActive(String token) {
			final Date expiration = getExpirationDateFromToken(token);			
			return new Date().before(expiration);
		}

		//generate token for user
		public LoginJwtToken generateToken(UserDetails userDetails) {
			Map<String, Object> claims = new HashMap<>();
			return doGenerateToken(claims, userDetails.getUsername());
		}

		private boolean isSessionAlive(String token) {
			SessionDTO dto;
			try {
				dto = sessionDAO.get(token);
				return SessionStatusEnum.ALIVE.toString().equals(dto.getStatus());
			} catch (Exception e) {
				logger.error("Error validating session status with token",e);
			} 
			return false;
			
		}

		private LoginJwtToken doGenerateToken(Map<String, Object> claims, String subject) {
			Date expireDate = new Date(System.currentTimeMillis() + TOKEN_VALIDITY * 1000);
			return new LoginJwtToken(Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(expireDate)
					.signWith(SignatureAlgorithm.HS512, secret).compact(),expireDate);
		}

		//validate token
		public Boolean validateToken(String token, UserDetails userDetails) {
			final String username = getUsernameFromToken(token);
			return (username.equals(userDetails.getUsername()) && isTokenActive(token) && isSessionAlive(token));
		}
	
}
