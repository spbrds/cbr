package pt.cbr.news.service.user;

import java.util.concurrent.ConcurrentHashMap;

import org.jboss.logging.Logger;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import pt.cbr.news.data.dao.UserDAO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.exceptions.NotFoundException;

@Component
public class UserCache {

	private ConcurrentHashMap<Integer, UserDTO> users = new ConcurrentHashMap<>();

	Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UserDAO userDAO;
	
	public UserDTO getUser(Integer userId) {
		return users.computeIfAbsent(userId, i -> {
			try {
				logger.info(String.format("Getting user %d to cache", userId));
				return userDAO.get(i).orElse(null);
			} catch (NotFoundException | DataAccessException e) {
				logger.error("Error getting user from database to cache", e);
			} catch(Exception ex) {
				logger.error("Generic error updating user cache",ex);
			}
			return null;
		});
	}
	
	public void putUser(UserDTO user) {
		users.putIfAbsent(user.getId(), user);
	}
	
	@Scheduled(cron = "${userpurge.scheduling}")
	//cleaning cache periodically - simplistic approach
	public void purgeCache(){
		users = new ConcurrentHashMap<>();		
	}
}
