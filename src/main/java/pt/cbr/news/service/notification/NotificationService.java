package pt.cbr.news.service.notification;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import pt.cbr.news.data.dao.NewsNotificationDAO;
import pt.cbr.news.data.dto.NewsItemDTO;
import pt.cbr.news.data.dto.NewsNotificationDTO;
import pt.cbr.news.data.dto.enums.NewsNotificationEvent;

@Service
@Transactional(rollbackOn = Throwable.class)
public class NotificationService{

	@Autowired
	private NewsNotificationDAO dao;
	
	@Autowired
	private UpdateNotificationProcessService updateService;
	
	@Autowired
	private UpvoteNotificationProcessService upvoteService;
	
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	public void registerItemUpVoteTreshold(@NonNull NewsItemDTO dto) {
		logger.info(String.format("Registering notification for upvote on new %d", dto.getId()));
		this.registerNotification(dto,NewsNotificationEvent.UPVOTE.toString());
	}
	
	public void registerItemUpdate(@NonNull NewsItemDTO dto) {
		logger.info(String.format("Registering notification for update on new %d", dto.getId()));
		this.registerNotification(dto,NewsNotificationEvent.UPDATE.toString());	
	}
	

	private void registerNotification(NewsItemDTO dto, String event) {
		NewsNotificationDTO notification = new NewsNotificationDTO(dto.getId(), event);
		
		//no need to duplicate notifications, if exist
		if (dao.isExistingNotification(notification) > 0) {
			return;
		}
		
		dao.create(notification);
	}
	
	@Scheduled(cron = "${notification.scheduling}")
	public Integer processNotifications() throws Exception {
		logger.warn("Notification processing running");
		
		dao.getAllNotifications().parallelStream().forEach(notification ->{
			try {	
				if(NewsNotificationEvent.UPVOTE.toString().equals(notification.getNotificationEvent())) {
					upvoteService.execute(notification);
				}else{
					updateService.execute(notification);
				}
				dao.delete(notification.getId());
			} catch (Exception e) {
				logger.error(String.format("Unable to process %s notification for new '%d' ",notification.getNotificationEvent().toString(), notification.getNewId()),e);
			}
		});
		
		return null;
	}
}
