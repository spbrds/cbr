package pt.cbr.news.service.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import pt.cbr.news.data.dto.enums.NotificationTypeEnum;
import pt.cbr.news.service.AppService;
import pt.cbr.news.service.notification.input.Message;

@Service
public class SmsNotificationService implements AppService<Message,Void>{

	Logger logger = LoggerFactory.getLogger(getClass());


	@Override
	public Void execute(Message input) throws Exception {
		logger.info(String.format("Sending %s notification for device of user %d - %s, for news article %s", NotificationTypeEnum.SMS.toString(),input.getUserDTO().getId(),input.getUserDTO().getName(),input.getNotificationDTO().getTitle()));
		return null;
	}

}
