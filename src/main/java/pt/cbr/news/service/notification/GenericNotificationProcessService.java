package pt.cbr.news.service.notification;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import pt.cbr.news.data.dto.UserNotificationDTO;
import pt.cbr.news.data.dto.enums.NotificationTypeEnum;
import pt.cbr.news.service.notification.dto.NewsItemNotificationDTO;
import pt.cbr.news.service.user.UserCache;

public class GenericNotificationProcessService {
	
	Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private ProcessNotificationService notificationService;
	
	@Autowired
	private UserCache userCache;

	protected void processNotificaion(List<UserNotificationDTO> nots, NewsItemNotificationDTO input) {
			nots.forEach(notification -> {
				try {
					NotificationTypeEnum type = NotificationTypeEnum.valueOf(notification.getNotificationType());
					notificationService.processNotification(input, userCache.getUser(notification.getUserId()), type);
				}catch(Exception e) {
					logger.error(String.format("Error on %s Notification Processor", input.getNotificationEvent()),e);
				}
			});
	}
}
