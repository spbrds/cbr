package pt.cbr.news.service.notification;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.cbr.news.data.dao.UserNotificationDAO;
import pt.cbr.news.data.dto.UserNotificationDTO;
import pt.cbr.news.service.AppService;
import pt.cbr.news.service.notification.dto.NewsItemNotificationDTO;

/*Implement the rules of upvote notification, who should be notified*/
@Service
public class UpvoteNotificationProcessService extends GenericNotificationProcessService implements AppService<NewsItemNotificationDTO,Void>{

	@Autowired
	private UserNotificationDAO notificationDAO;
	
	@Override
	public Void execute(NewsItemNotificationDTO input) throws Exception {
		
		//getting all users to notify upvotation
		List<UserNotificationDTO> nots = notificationDAO.getAllNotifiableUsers();
		
		this.processNotificaion(nots, input);
		
		return null;
		
	}

}
