package pt.cbr.news.service.notification;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.data.dto.enums.NotificationTypeEnum;
import pt.cbr.news.service.notification.dto.NewsItemNotificationDTO;
import pt.cbr.news.service.notification.input.Message;

@Service
public class ProcessNotificationService {
	
	Logger logger = Logger.getLogger(getClass());

	@Autowired
	private PushNotificationService push;
	
	@Autowired
	private SmsNotificationService sms;
	
	@Autowired
	private EmailNotificationService email;
	
	protected void processNotification(NewsItemNotificationDTO notificationDTO, UserDTO userDTO, NotificationTypeEnum type){
		try {	
			switch(type) {
			case EMAIL:
				email.execute(new Message(notificationDTO, userDTO));
				break;
			case SMS:
				sms.execute(new Message(notificationDTO, userDTO));
				break;
			case PUSH:
				push.execute(new Message(notificationDTO, userDTO));
				break;
			default:
				break;
			}
			
		} catch (Exception e) {
			logger.error("Error processing notification: "+notificationDTO.getNewId(),e);
		}
	}
}
