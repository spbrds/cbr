package pt.cbr.news.service.notification.input;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.notification.dto.NewsItemNotificationDTO;


@Getter @Setter
public class Message {
	
	public Message(@NonNull NewsItemNotificationDTO notificationDTO, @NonNull UserDTO userDTO) {
		super();
		this.notificationDTO = notificationDTO;
		this.userDTO = userDTO;
	}
	
	@NonNull
	private NewsItemNotificationDTO notificationDTO;
	@NonNull
	private UserDTO userDTO;
}
