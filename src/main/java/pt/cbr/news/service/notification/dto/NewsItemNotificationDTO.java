package pt.cbr.news.service.notification.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import pt.cbr.news.data.dto.NewsNotificationDTO;

@Getter @Setter
public class NewsItemNotificationDTO extends NewsNotificationDTO{

	public NewsItemNotificationDTO(@NonNull Integer newId, @NonNull String notificationEvent, String title, String body) {
		super(newId, notificationEvent);
		this.title = title;
		this.body = body;
		this.url = "htto://localhost:8080/mynewsserver/"+newId;
	}

	private String title;
	private String body;
	private String url;
	
	@Override
	public String toString() {
		return "NewsItemNotification [title=" + title + ", body=" + body + ", url=" + url + "]";
	}	
}
