package pt.cbr.news.service.audit;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import pt.cbr.news.data.dao.NewsAuditDAO;
import pt.cbr.news.data.dto.NewsAuditDTO;
import pt.cbr.news.data.dto.enums.AuditActionEnum;

@Service
public class AuditService {
	
	@Autowired
	private NewsAuditDAO dao;
	
	public void create(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.CREATE);
	}
	
	public void update(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.UPDATE);
	}
	
	public void delete(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.DELETE);
	}
	
	public void read(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.READ);
	}
	
	public void publish(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.PUBLISH);
	}
	
	public void upVote(@NonNull Integer newId, @NonNull Integer userId) {
		registerAuditAction(newId, userId, AuditActionEnum.UPVOTE);
	}
	
	private void registerAuditAction(Integer newId, Integer userId, AuditActionEnum action) {
		NewsAuditDTO dto = new NewsAuditDTO();
		dto.setUserId(userId);
		dto.setNewsItemId(newId);
		dto.setAuditAction(action.toString());
		dto.setCreateDate(LocalDateTime.now());
		dao.create(dto);
	}

}
