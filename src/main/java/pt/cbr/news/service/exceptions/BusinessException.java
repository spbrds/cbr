package pt.cbr.news.service.exceptions;

import org.springframework.lang.NonNull;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private final static Integer CODE = 400;
	
	public BusinessException(@NonNull String message) {
		super(message);
	}
	
	public Integer getCode() {
		return CODE;
	};
}
