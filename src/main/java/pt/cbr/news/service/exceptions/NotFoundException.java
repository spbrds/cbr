package pt.cbr.news.service.exceptions;

import org.springframework.lang.NonNull;

public class NotFoundException extends BusinessException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Integer CODE = 404;
	

	public NotFoundException(@NonNull String message) {
		super(message);
	}
	
	@Override
	public Integer getCode() {
		return CODE;
	}
	
	
}
