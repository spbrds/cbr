package pt.cbr.news.service.exceptions;

import org.springframework.lang.NonNull;

public class UnauthorizedException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Integer CODE = 401;
	
	public UnauthorizedException(@NonNull String message) {
		super(message);
	}

	@Override
	public Integer getCode() {
		return CODE;
	}

}
