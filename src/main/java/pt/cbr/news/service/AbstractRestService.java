package pt.cbr.news.service;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import pt.cbr.news.data.dao.UserDAO;
import pt.cbr.news.data.dto.UserDTO;
import pt.cbr.news.service.exceptions.BusinessException;
import pt.cbr.news.service.exceptions.UnauthorizedException;
import pt.cbr.news.service.response.ErrorResponse;
import pt.cbr.news.service.response.Response;
import pt.cbr.news.service.response.SuccessResponse;

public abstract class AbstractRestService<I,O> implements AppService<I,O>{
	
	protected Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UserDAO userDAO;
	
	public Response process(I input) {
		O serviceResult;
		
		try {
			//everything ok, execute operation
			serviceResult = this.execute(input);
			
			return new SuccessResponse<O>(200, "OK", serviceResult);
		}catch(BusinessException e){
			e.printStackTrace();
			return new ErrorResponse(e.getCode(),"NOK",e.getMessage());
		}catch(Exception e) {
			e.printStackTrace();
			return new ErrorResponse(500,"NOK","Internal Server error. Please try again later...");
		}
	}
	
	/*We could implement a cash for this service, to spare Database load. No need to access every time*/
	protected UserDTO getAuthenticatedUser() throws UnauthorizedException {
		
		return userDAO.getUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName()) .orElseThrow(() -> new UnauthorizedException("Unauthorized"));
	}
	
}
