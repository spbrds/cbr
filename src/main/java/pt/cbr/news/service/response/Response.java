package pt.cbr.news.service.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Response {
	
	
	private Integer code;
	private String status;
	
		
	public Response(Integer code, String status) {
		this.code = code;
		this.status = status;
	}
	
}
