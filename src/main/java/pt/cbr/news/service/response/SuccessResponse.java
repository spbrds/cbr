package pt.cbr.news.service.response;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SuccessResponse<O> extends Response {
	
	public SuccessResponse(@NonNull Integer code, String status, O content) {
		super(code, status);
		this.content = content;
	}

	private O content;
	
}
