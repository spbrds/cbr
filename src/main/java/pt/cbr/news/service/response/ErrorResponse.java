package pt.cbr.news.service.response;

import org.springframework.lang.NonNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ErrorResponse extends Response{
	
	
	private String text;
	
	public ErrorResponse(@NonNull Integer code, String status, String text) {
		super(code, status);
		this.text = text;
	}

}
