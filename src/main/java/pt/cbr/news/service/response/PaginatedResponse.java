package pt.cbr.news.service.response;

import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter @Setter
public class PaginatedResponse<O>{

	@NonNull
	private Integer pageNumber;
	@NonNull
	private Integer pageSize;
	@NonNull
	private Integer totalRecords;
	@NonNull
	private List<O> data;
}
