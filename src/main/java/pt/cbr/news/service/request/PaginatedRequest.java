package pt.cbr.news.service.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter @Setter
public class PaginatedRequest<T> {
	
	@NonNull
	private Integer pageNumber;
	
	@NonNull
	private Integer pageSize;
	
	private T filter;

}
