package pt.cbr.news.service;


public interface AppService<I,O> {
	
	public O execute(I input) throws Exception;
	
}
