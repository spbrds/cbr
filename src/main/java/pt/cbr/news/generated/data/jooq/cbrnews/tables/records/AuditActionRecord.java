/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.cbrnews.tables.records;


import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Row1;
import org.jooq.impl.UpdatableRecordImpl;

import pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditAction;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AuditActionRecord extends UpdatableRecordImpl<AuditActionRecord> implements Record1<String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>cbrnews.audit_action.name</code>.
     */
    public void setName(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>cbrnews.audit_action.name</code>.
     */
    public String getName() {
        return (String) get(0);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record1 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row1<String> fieldsRow() {
        return (Row1) super.fieldsRow();
    }

    @Override
    public Row1<String> valuesRow() {
        return (Row1) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return AuditAction.AUDIT_ACTION.NAME;
    }

    @Override
    public String component1() {
        return getName();
    }

    @Override
    public String value1() {
        return getName();
    }

    @Override
    public AuditActionRecord value1(String value) {
        setName(value);
        return this;
    }

    @Override
    public AuditActionRecord values(String value1) {
        value1(value1);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AuditActionRecord
     */
    public AuditActionRecord() {
        super(AuditAction.AUDIT_ACTION);
    }

    /**
     * Create a detached, initialised AuditActionRecord
     */
    public AuditActionRecord(String name) {
        super(AuditAction.AUDIT_ACTION);

        setName(name);
    }
}
