/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.information_schema.tables;


import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row8;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.information_schema.InformationSchema;
import pt.cbr.news.generated.data.jooq.information_schema.tables.records.TablePrivilegesRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TablePrivileges extends TableImpl<TablePrivilegesRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>information_schema.table_privileges</code>
     */
    public static final TablePrivileges TABLE_PRIVILEGES = new TablePrivileges();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TablePrivilegesRecord> getRecordType() {
        return TablePrivilegesRecord.class;
    }

    /**
     * The column <code>information_schema.table_privileges.grantor</code>.
     */
    public final TableField<TablePrivilegesRecord, String> GRANTOR = createField(DSL.name("grantor"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.grantee</code>.
     */
    public final TableField<TablePrivilegesRecord, String> GRANTEE = createField(DSL.name("grantee"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.table_catalog</code>.
     */
    public final TableField<TablePrivilegesRecord, String> TABLE_CATALOG = createField(DSL.name("table_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.table_schema</code>.
     */
    public final TableField<TablePrivilegesRecord, String> TABLE_SCHEMA = createField(DSL.name("table_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.table_name</code>.
     */
    public final TableField<TablePrivilegesRecord, String> TABLE_NAME = createField(DSL.name("table_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.privilege_type</code>.
     */
    public final TableField<TablePrivilegesRecord, String> PRIVILEGE_TYPE = createField(DSL.name("privilege_type"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.is_grantable</code>.
     */
    public final TableField<TablePrivilegesRecord, String> IS_GRANTABLE = createField(DSL.name("is_grantable"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_privileges.with_hierarchy</code>.
     */
    public final TableField<TablePrivilegesRecord, String> WITH_HIERARCHY = createField(DSL.name("with_hierarchy"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    private TablePrivileges(Name alias, Table<TablePrivilegesRecord> aliased) {
        this(alias, aliased, null);
    }

    private TablePrivileges(Name alias, Table<TablePrivilegesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    /**
     * Create an aliased <code>information_schema.table_privileges</code> table reference
     */
    public TablePrivileges(String alias) {
        this(DSL.name(alias), TABLE_PRIVILEGES);
    }

    /**
     * Create an aliased <code>information_schema.table_privileges</code> table reference
     */
    public TablePrivileges(Name alias) {
        this(alias, TABLE_PRIVILEGES);
    }

    /**
     * Create a <code>information_schema.table_privileges</code> table reference
     */
    public TablePrivileges() {
        this(DSL.name("table_privileges"), null);
    }

    public <O extends Record> TablePrivileges(Table<O> child, ForeignKey<O, TablePrivilegesRecord> key) {
        super(child, key, TABLE_PRIVILEGES);
    }

    @Override
    public Schema getSchema() {
        return InformationSchema.INFORMATION_SCHEMA;
    }

    @Override
    public TablePrivileges as(String alias) {
        return new TablePrivileges(DSL.name(alias), this);
    }

    @Override
    public TablePrivileges as(Name alias) {
        return new TablePrivileges(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TablePrivileges rename(String name) {
        return new TablePrivileges(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TablePrivileges rename(Name name) {
        return new TablePrivileges(name, null);
    }

    // -------------------------------------------------------------------------
    // Row8 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row8<String, String, String, String, String, String, String, String> fieldsRow() {
        return (Row8) super.fieldsRow();
    }
}
