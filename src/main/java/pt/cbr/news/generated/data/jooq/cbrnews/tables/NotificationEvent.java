/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.cbrnews.tables;


import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row1;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.cbrnews.Cbrnews;
import pt.cbr.news.generated.data.jooq.cbrnews.Keys;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.records.NotificationEventRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class NotificationEvent extends TableImpl<NotificationEventRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>cbrnews.notification_event</code>
     */
    public static final NotificationEvent NOTIFICATION_EVENT = new NotificationEvent();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<NotificationEventRecord> getRecordType() {
        return NotificationEventRecord.class;
    }

    /**
     * The column <code>cbrnews.notification_event.name</code>.
     */
    public final TableField<NotificationEventRecord, String> NAME = createField(DSL.name("name"), SQLDataType.VARCHAR(10).nullable(false), this, "");

    private NotificationEvent(Name alias, Table<NotificationEventRecord> aliased) {
        this(alias, aliased, null);
    }

    private NotificationEvent(Name alias, Table<NotificationEventRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>cbrnews.notification_event</code> table reference
     */
    public NotificationEvent(String alias) {
        this(DSL.name(alias), NOTIFICATION_EVENT);
    }

    /**
     * Create an aliased <code>cbrnews.notification_event</code> table reference
     */
    public NotificationEvent(Name alias) {
        this(alias, NOTIFICATION_EVENT);
    }

    /**
     * Create a <code>cbrnews.notification_event</code> table reference
     */
    public NotificationEvent() {
        this(DSL.name("notification_event"), null);
    }

    public <O extends Record> NotificationEvent(Table<O> child, ForeignKey<O, NotificationEventRecord> key) {
        super(child, key, NOTIFICATION_EVENT);
    }

    @Override
    public Schema getSchema() {
        return Cbrnews.CBRNEWS;
    }

    @Override
    public UniqueKey<NotificationEventRecord> getPrimaryKey() {
        return Keys.NOTIFICATION_EVENT_PKEY;
    }

    @Override
    public List<UniqueKey<NotificationEventRecord>> getKeys() {
        return Arrays.<UniqueKey<NotificationEventRecord>>asList(Keys.NOTIFICATION_EVENT_PKEY);
    }

    @Override
    public NotificationEvent as(String alias) {
        return new NotificationEvent(DSL.name(alias), this);
    }

    @Override
    public NotificationEvent as(Name alias) {
        return new NotificationEvent(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public NotificationEvent rename(String name) {
        return new NotificationEvent(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public NotificationEvent rename(Name name) {
        return new NotificationEvent(name, null);
    }

    // -------------------------------------------------------------------------
    // Row1 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row1<String> fieldsRow() {
        return (Row1) super.fieldsRow();
    }
}
