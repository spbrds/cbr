/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.cbrnews;


import pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditAction;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.AuditNews;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.New;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.NewStatus;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.NewsPendingNotification;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.NotificationEvent;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.NotificationMethod;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.Session;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.User;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.UserNotification;


/**
 * Convenience access to all tables in cbrnews.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>cbrnews.audit_action</code>.
     */
    public static final AuditAction AUDIT_ACTION = AuditAction.AUDIT_ACTION;

    /**
     * The table <code>cbrnews.audit_news</code>.
     */
    public static final AuditNews AUDIT_NEWS = AuditNews.AUDIT_NEWS;

    /**
     * The table <code>cbrnews.new</code>.
     */
    public static final New NEW = New.NEW;

    /**
     * The table <code>cbrnews.new_status</code>.
     */
    public static final NewStatus NEW_STATUS = NewStatus.NEW_STATUS;

    /**
     * The table <code>cbrnews.news_pending_notification</code>.
     */
    public static final NewsPendingNotification NEWS_PENDING_NOTIFICATION = NewsPendingNotification.NEWS_PENDING_NOTIFICATION;

    /**
     * The table <code>cbrnews.notification_event</code>.
     */
    public static final NotificationEvent NOTIFICATION_EVENT = NotificationEvent.NOTIFICATION_EVENT;

    /**
     * The table <code>cbrnews.notification_method</code>.
     */
    public static final NotificationMethod NOTIFICATION_METHOD = NotificationMethod.NOTIFICATION_METHOD;

    /**
     * The table <code>cbrnews.session</code>.
     */
    public static final Session SESSION = Session.SESSION;

    /**
     * The table <code>cbrnews.user</code>.
     */
    public static final User USER = User.USER;

    /**
     * The table <code>cbrnews.user_notification</code>.
     */
    public static final UserNotification USER_NOTIFICATION = UserNotification.USER_NOTIFICATION;
}
