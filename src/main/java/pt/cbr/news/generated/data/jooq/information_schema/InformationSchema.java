/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.information_schema;


import java.util.Arrays;
import java.util.List;

import org.jooq.Catalog;
import org.jooq.Configuration;
import org.jooq.Domain;
import org.jooq.Field;
import org.jooq.Result;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;

import pt.cbr.news.generated.data.jooq.DefaultCatalog;
import pt.cbr.news.generated.data.jooq.information_schema.tables.AdministrableRoleAuthorizations;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ApplicableRoles;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Attributes;
import pt.cbr.news.generated.data.jooq.information_schema.tables.CharacterSets;
import pt.cbr.news.generated.data.jooq.information_schema.tables.CheckConstraintRoutineUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.CheckConstraints;
import pt.cbr.news.generated.data.jooq.information_schema.tables.CollationCharacterSetApplicability;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Collations;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ColumnDomainUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ColumnOptions;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ColumnPrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ColumnUdtUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Columns;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ConstraintColumnUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ConstraintTableUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.DataTypePrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.DomainConstraints;
import pt.cbr.news.generated.data.jooq.information_schema.tables.DomainUdtUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Domains;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ElementTypes;
import pt.cbr.news.generated.data.jooq.information_schema.tables.EnabledRoles;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignDataWrapperOptions;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignDataWrappers;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignServerOptions;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignServers;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignTableOptions;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ForeignTables;
import pt.cbr.news.generated.data.jooq.information_schema.tables.InformationSchemaCatalogName;
import pt.cbr.news.generated.data.jooq.information_schema.tables.KeyColumnUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Parameters;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ReferentialConstraints;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoleColumnGrants;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoleRoutineGrants;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoleTableGrants;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoleUdtGrants;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoleUsageGrants;
import pt.cbr.news.generated.data.jooq.information_schema.tables.RoutinePrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Routines;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Schemata;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Sequences;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlFeatures;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlImplementationInfo;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlLanguages;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlPackages;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlSizing;
import pt.cbr.news.generated.data.jooq.information_schema.tables.SqlSizingProfiles;
import pt.cbr.news.generated.data.jooq.information_schema.tables.TableConstraints;
import pt.cbr.news.generated.data.jooq.information_schema.tables.TablePrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Tables;
import pt.cbr.news.generated.data.jooq.information_schema.tables.TriggeredUpdateColumns;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Triggers;
import pt.cbr.news.generated.data.jooq.information_schema.tables.UdtPrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.UsagePrivileges;
import pt.cbr.news.generated.data.jooq.information_schema.tables.UserDefinedTypes;
import pt.cbr.news.generated.data.jooq.information_schema.tables.UserMappingOptions;
import pt.cbr.news.generated.data.jooq.information_schema.tables.UserMappings;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ViewColumnUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ViewRoutineUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.ViewTableUsage;
import pt.cbr.news.generated.data.jooq.information_schema.tables.Views;
import pt.cbr.news.generated.data.jooq.information_schema.tables._PgExpandarray;
import pt.cbr.news.generated.data.jooq.information_schema.tables.records._PgExpandarrayRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InformationSchema extends SchemaImpl {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>information_schema</code>
     */
    public static final InformationSchema INFORMATION_SCHEMA = new InformationSchema();

    /**
     * The table <code>information_schema._pg_expandarray</code>.
     */
    public final _PgExpandarray _PG_EXPANDARRAY = _PgExpandarray._PG_EXPANDARRAY;

    /**
     * @deprecated Unknown data type. Please define an explicit {@link org.jooq.Binding} to specify how this type should be handled. Deprecation can be turned off using {@literal <deprecationOnUnknownTypes/>} in your code generator configuration.
     */
    @Deprecated
    public static Result<_PgExpandarrayRecord> _PG_EXPANDARRAY(
          Configuration configuration
        , Object __1
    ) {
        return configuration.dsl().selectFrom(pt.cbr.news.generated.data.jooq.information_schema.tables._PgExpandarray._PG_EXPANDARRAY.call(
              __1
        )).fetch();
    }

    /**
     * @deprecated Unknown data type. Please define an explicit {@link org.jooq.Binding} to specify how this type should be handled. Deprecation can be turned off using {@literal <deprecationOnUnknownTypes/>} in your code generator configuration.
     */
    @Deprecated
    public static _PgExpandarray _PG_EXPANDARRAY(
          Object __1
    ) {
        return pt.cbr.news.generated.data.jooq.information_schema.tables._PgExpandarray._PG_EXPANDARRAY.call(
              __1
        );
    }

    /**
     * @deprecated Unknown data type. Please define an explicit {@link org.jooq.Binding} to specify how this type should be handled. Deprecation can be turned off using {@literal <deprecationOnUnknownTypes/>} in your code generator configuration.
     */
    @Deprecated
    public static _PgExpandarray _PG_EXPANDARRAY(
          Field<Object> __1
    ) {
        return pt.cbr.news.generated.data.jooq.information_schema.tables._PgExpandarray._PG_EXPANDARRAY.call(
              __1
        );
    }

    /**
     * The table <code>information_schema.administrable_role_authorizations</code>.
     */
    public final AdministrableRoleAuthorizations ADMINISTRABLE_ROLE_AUTHORIZATIONS = AdministrableRoleAuthorizations.ADMINISTRABLE_ROLE_AUTHORIZATIONS;

    /**
     * The table <code>information_schema.applicable_roles</code>.
     */
    public final ApplicableRoles APPLICABLE_ROLES = ApplicableRoles.APPLICABLE_ROLES;

    /**
     * The table <code>information_schema.attributes</code>.
     */
    public final Attributes ATTRIBUTES = Attributes.ATTRIBUTES;

    /**
     * The table <code>information_schema.character_sets</code>.
     */
    public final CharacterSets CHARACTER_SETS = CharacterSets.CHARACTER_SETS;

    /**
     * The table <code>information_schema.check_constraint_routine_usage</code>.
     */
    public final CheckConstraintRoutineUsage CHECK_CONSTRAINT_ROUTINE_USAGE = CheckConstraintRoutineUsage.CHECK_CONSTRAINT_ROUTINE_USAGE;

    /**
     * The table <code>information_schema.check_constraints</code>.
     */
    public final CheckConstraints CHECK_CONSTRAINTS = CheckConstraints.CHECK_CONSTRAINTS;

    /**
     * The table <code>information_schema.collation_character_set_applicability</code>.
     */
    public final CollationCharacterSetApplicability COLLATION_CHARACTER_SET_APPLICABILITY = CollationCharacterSetApplicability.COLLATION_CHARACTER_SET_APPLICABILITY;

    /**
     * The table <code>information_schema.collations</code>.
     */
    public final Collations COLLATIONS = Collations.COLLATIONS;

    /**
     * The table <code>information_schema.column_domain_usage</code>.
     */
    public final ColumnDomainUsage COLUMN_DOMAIN_USAGE = ColumnDomainUsage.COLUMN_DOMAIN_USAGE;

    /**
     * The table <code>information_schema.column_options</code>.
     */
    public final ColumnOptions COLUMN_OPTIONS = ColumnOptions.COLUMN_OPTIONS;

    /**
     * The table <code>information_schema.column_privileges</code>.
     */
    public final ColumnPrivileges COLUMN_PRIVILEGES = ColumnPrivileges.COLUMN_PRIVILEGES;

    /**
     * The table <code>information_schema.column_udt_usage</code>.
     */
    public final ColumnUdtUsage COLUMN_UDT_USAGE = ColumnUdtUsage.COLUMN_UDT_USAGE;

    /**
     * The table <code>information_schema.columns</code>.
     */
    public final Columns COLUMNS = Columns.COLUMNS;

    /**
     * The table <code>information_schema.constraint_column_usage</code>.
     */
    public final ConstraintColumnUsage CONSTRAINT_COLUMN_USAGE = ConstraintColumnUsage.CONSTRAINT_COLUMN_USAGE;

    /**
     * The table <code>information_schema.constraint_table_usage</code>.
     */
    public final ConstraintTableUsage CONSTRAINT_TABLE_USAGE = ConstraintTableUsage.CONSTRAINT_TABLE_USAGE;

    /**
     * The table <code>information_schema.data_type_privileges</code>.
     */
    public final DataTypePrivileges DATA_TYPE_PRIVILEGES = DataTypePrivileges.DATA_TYPE_PRIVILEGES;

    /**
     * The table <code>information_schema.domain_constraints</code>.
     */
    public final DomainConstraints DOMAIN_CONSTRAINTS = DomainConstraints.DOMAIN_CONSTRAINTS;

    /**
     * The table <code>information_schema.domain_udt_usage</code>.
     */
    public final DomainUdtUsage DOMAIN_UDT_USAGE = DomainUdtUsage.DOMAIN_UDT_USAGE;

    /**
     * The table <code>information_schema.domains</code>.
     */
    public final Domains DOMAINS = Domains.DOMAINS;

    /**
     * The table <code>information_schema.element_types</code>.
     */
    public final ElementTypes ELEMENT_TYPES = ElementTypes.ELEMENT_TYPES;

    /**
     * The table <code>information_schema.enabled_roles</code>.
     */
    public final EnabledRoles ENABLED_ROLES = EnabledRoles.ENABLED_ROLES;

    /**
     * The table <code>information_schema.foreign_data_wrapper_options</code>.
     */
    public final ForeignDataWrapperOptions FOREIGN_DATA_WRAPPER_OPTIONS = ForeignDataWrapperOptions.FOREIGN_DATA_WRAPPER_OPTIONS;

    /**
     * The table <code>information_schema.foreign_data_wrappers</code>.
     */
    public final ForeignDataWrappers FOREIGN_DATA_WRAPPERS = ForeignDataWrappers.FOREIGN_DATA_WRAPPERS;

    /**
     * The table <code>information_schema.foreign_server_options</code>.
     */
    public final ForeignServerOptions FOREIGN_SERVER_OPTIONS = ForeignServerOptions.FOREIGN_SERVER_OPTIONS;

    /**
     * The table <code>information_schema.foreign_servers</code>.
     */
    public final ForeignServers FOREIGN_SERVERS = ForeignServers.FOREIGN_SERVERS;

    /**
     * The table <code>information_schema.foreign_table_options</code>.
     */
    public final ForeignTableOptions FOREIGN_TABLE_OPTIONS = ForeignTableOptions.FOREIGN_TABLE_OPTIONS;

    /**
     * The table <code>information_schema.foreign_tables</code>.
     */
    public final ForeignTables FOREIGN_TABLES = ForeignTables.FOREIGN_TABLES;

    /**
     * The table <code>information_schema.information_schema_catalog_name</code>.
     */
    public final InformationSchemaCatalogName INFORMATION_SCHEMA_CATALOG_NAME = InformationSchemaCatalogName.INFORMATION_SCHEMA_CATALOG_NAME;

    /**
     * The table <code>information_schema.key_column_usage</code>.
     */
    public final KeyColumnUsage KEY_COLUMN_USAGE = KeyColumnUsage.KEY_COLUMN_USAGE;

    /**
     * The table <code>information_schema.parameters</code>.
     */
    public final Parameters PARAMETERS = Parameters.PARAMETERS;

    /**
     * The table <code>information_schema.referential_constraints</code>.
     */
    public final ReferentialConstraints REFERENTIAL_CONSTRAINTS = ReferentialConstraints.REFERENTIAL_CONSTRAINTS;

    /**
     * The table <code>information_schema.role_column_grants</code>.
     */
    public final RoleColumnGrants ROLE_COLUMN_GRANTS = RoleColumnGrants.ROLE_COLUMN_GRANTS;

    /**
     * The table <code>information_schema.role_routine_grants</code>.
     */
    public final RoleRoutineGrants ROLE_ROUTINE_GRANTS = RoleRoutineGrants.ROLE_ROUTINE_GRANTS;

    /**
     * The table <code>information_schema.role_table_grants</code>.
     */
    public final RoleTableGrants ROLE_TABLE_GRANTS = RoleTableGrants.ROLE_TABLE_GRANTS;

    /**
     * The table <code>information_schema.role_udt_grants</code>.
     */
    public final RoleUdtGrants ROLE_UDT_GRANTS = RoleUdtGrants.ROLE_UDT_GRANTS;

    /**
     * The table <code>information_schema.role_usage_grants</code>.
     */
    public final RoleUsageGrants ROLE_USAGE_GRANTS = RoleUsageGrants.ROLE_USAGE_GRANTS;

    /**
     * The table <code>information_schema.routine_privileges</code>.
     */
    public final RoutinePrivileges ROUTINE_PRIVILEGES = RoutinePrivileges.ROUTINE_PRIVILEGES;

    /**
     * The table <code>information_schema.routines</code>.
     */
    public final Routines ROUTINES = Routines.ROUTINES;

    /**
     * The table <code>information_schema.schemata</code>.
     */
    public final Schemata SCHEMATA = Schemata.SCHEMATA;

    /**
     * The table <code>information_schema.sequences</code>.
     */
    public final Sequences SEQUENCES = Sequences.SEQUENCES;

    /**
     * The table <code>information_schema.sql_features</code>.
     */
    public final SqlFeatures SQL_FEATURES = SqlFeatures.SQL_FEATURES;

    /**
     * The table <code>information_schema.sql_implementation_info</code>.
     */
    public final SqlImplementationInfo SQL_IMPLEMENTATION_INFO = SqlImplementationInfo.SQL_IMPLEMENTATION_INFO;

    /**
     * The table <code>information_schema.sql_languages</code>.
     */
    public final SqlLanguages SQL_LANGUAGES = SqlLanguages.SQL_LANGUAGES;

    /**
     * The table <code>information_schema.sql_packages</code>.
     */
    public final SqlPackages SQL_PACKAGES = SqlPackages.SQL_PACKAGES;

    /**
     * The table <code>information_schema.sql_sizing</code>.
     */
    public final SqlSizing SQL_SIZING = SqlSizing.SQL_SIZING;

    /**
     * The table <code>information_schema.sql_sizing_profiles</code>.
     */
    public final SqlSizingProfiles SQL_SIZING_PROFILES = SqlSizingProfiles.SQL_SIZING_PROFILES;

    /**
     * The table <code>information_schema.table_constraints</code>.
     */
    public final TableConstraints TABLE_CONSTRAINTS = TableConstraints.TABLE_CONSTRAINTS;

    /**
     * The table <code>information_schema.table_privileges</code>.
     */
    public final TablePrivileges TABLE_PRIVILEGES = TablePrivileges.TABLE_PRIVILEGES;

    /**
     * The table <code>information_schema.tables</code>.
     */
    public final Tables TABLES = Tables.TABLES;

    /**
     * The table <code>information_schema.triggered_update_columns</code>.
     */
    public final TriggeredUpdateColumns TRIGGERED_UPDATE_COLUMNS = TriggeredUpdateColumns.TRIGGERED_UPDATE_COLUMNS;

    /**
     * The table <code>information_schema.triggers</code>.
     */
    public final Triggers TRIGGERS = Triggers.TRIGGERS;

    /**
     * The table <code>information_schema.udt_privileges</code>.
     */
    public final UdtPrivileges UDT_PRIVILEGES = UdtPrivileges.UDT_PRIVILEGES;

    /**
     * The table <code>information_schema.usage_privileges</code>.
     */
    public final UsagePrivileges USAGE_PRIVILEGES = UsagePrivileges.USAGE_PRIVILEGES;

    /**
     * The table <code>information_schema.user_defined_types</code>.
     */
    public final UserDefinedTypes USER_DEFINED_TYPES = UserDefinedTypes.USER_DEFINED_TYPES;

    /**
     * The table <code>information_schema.user_mapping_options</code>.
     */
    public final UserMappingOptions USER_MAPPING_OPTIONS = UserMappingOptions.USER_MAPPING_OPTIONS;

    /**
     * The table <code>information_schema.user_mappings</code>.
     */
    public final UserMappings USER_MAPPINGS = UserMappings.USER_MAPPINGS;

    /**
     * The table <code>information_schema.view_column_usage</code>.
     */
    public final ViewColumnUsage VIEW_COLUMN_USAGE = ViewColumnUsage.VIEW_COLUMN_USAGE;

    /**
     * The table <code>information_schema.view_routine_usage</code>.
     */
    public final ViewRoutineUsage VIEW_ROUTINE_USAGE = ViewRoutineUsage.VIEW_ROUTINE_USAGE;

    /**
     * The table <code>information_schema.view_table_usage</code>.
     */
    public final ViewTableUsage VIEW_TABLE_USAGE = ViewTableUsage.VIEW_TABLE_USAGE;

    /**
     * The table <code>information_schema.views</code>.
     */
    public final Views VIEWS = Views.VIEWS;

    /**
     * No further instances allowed
     */
    private InformationSchema() {
        super("information_schema", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Domain<?>> getDomains() {
        return Arrays.<Domain<?>>asList(
            pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER,
            pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA,
            pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER,
            pt.cbr.news.generated.data.jooq.information_schema.Domains.TIME_STAMP,
            pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO);
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.<Table<?>>asList(
            _PgExpandarray._PG_EXPANDARRAY,
            AdministrableRoleAuthorizations.ADMINISTRABLE_ROLE_AUTHORIZATIONS,
            ApplicableRoles.APPLICABLE_ROLES,
            Attributes.ATTRIBUTES,
            CharacterSets.CHARACTER_SETS,
            CheckConstraintRoutineUsage.CHECK_CONSTRAINT_ROUTINE_USAGE,
            CheckConstraints.CHECK_CONSTRAINTS,
            CollationCharacterSetApplicability.COLLATION_CHARACTER_SET_APPLICABILITY,
            Collations.COLLATIONS,
            ColumnDomainUsage.COLUMN_DOMAIN_USAGE,
            ColumnOptions.COLUMN_OPTIONS,
            ColumnPrivileges.COLUMN_PRIVILEGES,
            ColumnUdtUsage.COLUMN_UDT_USAGE,
            Columns.COLUMNS,
            ConstraintColumnUsage.CONSTRAINT_COLUMN_USAGE,
            ConstraintTableUsage.CONSTRAINT_TABLE_USAGE,
            DataTypePrivileges.DATA_TYPE_PRIVILEGES,
            DomainConstraints.DOMAIN_CONSTRAINTS,
            DomainUdtUsage.DOMAIN_UDT_USAGE,
            Domains.DOMAINS,
            ElementTypes.ELEMENT_TYPES,
            EnabledRoles.ENABLED_ROLES,
            ForeignDataWrapperOptions.FOREIGN_DATA_WRAPPER_OPTIONS,
            ForeignDataWrappers.FOREIGN_DATA_WRAPPERS,
            ForeignServerOptions.FOREIGN_SERVER_OPTIONS,
            ForeignServers.FOREIGN_SERVERS,
            ForeignTableOptions.FOREIGN_TABLE_OPTIONS,
            ForeignTables.FOREIGN_TABLES,
            InformationSchemaCatalogName.INFORMATION_SCHEMA_CATALOG_NAME,
            KeyColumnUsage.KEY_COLUMN_USAGE,
            Parameters.PARAMETERS,
            ReferentialConstraints.REFERENTIAL_CONSTRAINTS,
            RoleColumnGrants.ROLE_COLUMN_GRANTS,
            RoleRoutineGrants.ROLE_ROUTINE_GRANTS,
            RoleTableGrants.ROLE_TABLE_GRANTS,
            RoleUdtGrants.ROLE_UDT_GRANTS,
            RoleUsageGrants.ROLE_USAGE_GRANTS,
            RoutinePrivileges.ROUTINE_PRIVILEGES,
            Routines.ROUTINES,
            Schemata.SCHEMATA,
            Sequences.SEQUENCES,
            SqlFeatures.SQL_FEATURES,
            SqlImplementationInfo.SQL_IMPLEMENTATION_INFO,
            SqlLanguages.SQL_LANGUAGES,
            SqlPackages.SQL_PACKAGES,
            SqlSizing.SQL_SIZING,
            SqlSizingProfiles.SQL_SIZING_PROFILES,
            TableConstraints.TABLE_CONSTRAINTS,
            TablePrivileges.TABLE_PRIVILEGES,
            Tables.TABLES,
            TriggeredUpdateColumns.TRIGGERED_UPDATE_COLUMNS,
            Triggers.TRIGGERS,
            UdtPrivileges.UDT_PRIVILEGES,
            UsagePrivileges.USAGE_PRIVILEGES,
            UserDefinedTypes.USER_DEFINED_TYPES,
            UserMappingOptions.USER_MAPPING_OPTIONS,
            UserMappings.USER_MAPPINGS,
            ViewColumnUsage.VIEW_COLUMN_USAGE,
            ViewRoutineUsage.VIEW_ROUTINE_USAGE,
            ViewTableUsage.VIEW_TABLE_USAGE,
            Views.VIEWS);
    }
}
