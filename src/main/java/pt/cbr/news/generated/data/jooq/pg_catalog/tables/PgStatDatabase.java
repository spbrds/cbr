/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.pg_catalog.tables;


import java.time.OffsetDateTime;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row19;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.pg_catalog.PgCatalog;
import pt.cbr.news.generated.data.jooq.pg_catalog.tables.records.PgStatDatabaseRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PgStatDatabase extends TableImpl<PgStatDatabaseRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>pg_catalog.pg_stat_database</code>
     */
    public static final PgStatDatabase PG_STAT_DATABASE = new PgStatDatabase();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PgStatDatabaseRecord> getRecordType() {
        return PgStatDatabaseRecord.class;
    }

    /**
     * The column <code>pg_catalog.pg_stat_database.datid</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> DATID = createField(DSL.name("datid"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.datname</code>.
     */
    public final TableField<PgStatDatabaseRecord, String> DATNAME = createField(DSL.name("datname"), SQLDataType.VARCHAR, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.numbackends</code>.
     */
    public final TableField<PgStatDatabaseRecord, Integer> NUMBACKENDS = createField(DSL.name("numbackends"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.xact_commit</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> XACT_COMMIT = createField(DSL.name("xact_commit"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.xact_rollback</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> XACT_ROLLBACK = createField(DSL.name("xact_rollback"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.blks_read</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> BLKS_READ = createField(DSL.name("blks_read"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.blks_hit</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> BLKS_HIT = createField(DSL.name("blks_hit"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.tup_returned</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TUP_RETURNED = createField(DSL.name("tup_returned"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.tup_fetched</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TUP_FETCHED = createField(DSL.name("tup_fetched"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.tup_inserted</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TUP_INSERTED = createField(DSL.name("tup_inserted"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.tup_updated</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TUP_UPDATED = createField(DSL.name("tup_updated"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.tup_deleted</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TUP_DELETED = createField(DSL.name("tup_deleted"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.conflicts</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> CONFLICTS = createField(DSL.name("conflicts"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.temp_files</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TEMP_FILES = createField(DSL.name("temp_files"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.temp_bytes</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> TEMP_BYTES = createField(DSL.name("temp_bytes"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.deadlocks</code>.
     */
    public final TableField<PgStatDatabaseRecord, Long> DEADLOCKS = createField(DSL.name("deadlocks"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.blk_read_time</code>.
     */
    public final TableField<PgStatDatabaseRecord, Double> BLK_READ_TIME = createField(DSL.name("blk_read_time"), SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.blk_write_time</code>.
     */
    public final TableField<PgStatDatabaseRecord, Double> BLK_WRITE_TIME = createField(DSL.name("blk_write_time"), SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>pg_catalog.pg_stat_database.stats_reset</code>.
     */
    public final TableField<PgStatDatabaseRecord, OffsetDateTime> STATS_RESET = createField(DSL.name("stats_reset"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "");

    private PgStatDatabase(Name alias, Table<PgStatDatabaseRecord> aliased) {
        this(alias, aliased, null);
    }

    private PgStatDatabase(Name alias, Table<PgStatDatabaseRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    /**
     * Create an aliased <code>pg_catalog.pg_stat_database</code> table reference
     */
    public PgStatDatabase(String alias) {
        this(DSL.name(alias), PG_STAT_DATABASE);
    }

    /**
     * Create an aliased <code>pg_catalog.pg_stat_database</code> table reference
     */
    public PgStatDatabase(Name alias) {
        this(alias, PG_STAT_DATABASE);
    }

    /**
     * Create a <code>pg_catalog.pg_stat_database</code> table reference
     */
    public PgStatDatabase() {
        this(DSL.name("pg_stat_database"), null);
    }

    public <O extends Record> PgStatDatabase(Table<O> child, ForeignKey<O, PgStatDatabaseRecord> key) {
        super(child, key, PG_STAT_DATABASE);
    }

    @Override
    public Schema getSchema() {
        return PgCatalog.PG_CATALOG;
    }

    @Override
    public PgStatDatabase as(String alias) {
        return new PgStatDatabase(DSL.name(alias), this);
    }

    @Override
    public PgStatDatabase as(Name alias) {
        return new PgStatDatabase(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public PgStatDatabase rename(String name) {
        return new PgStatDatabase(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public PgStatDatabase rename(Name name) {
        return new PgStatDatabase(name, null);
    }

    // -------------------------------------------------------------------------
    // Row19 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row19<Long, String, Integer, Long, Long, Long, Long, Long, Long, Long, Long, Long, Long, Long, Long, Long, Double, Double, OffsetDateTime> fieldsRow() {
        return (Row19) super.fieldsRow();
    }
}
