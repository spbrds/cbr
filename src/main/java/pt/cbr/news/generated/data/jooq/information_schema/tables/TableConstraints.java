/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.information_schema.tables;


import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row9;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.information_schema.InformationSchema;
import pt.cbr.news.generated.data.jooq.information_schema.tables.records.TableConstraintsRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TableConstraints extends TableImpl<TableConstraintsRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>information_schema.table_constraints</code>
     */
    public static final TableConstraints TABLE_CONSTRAINTS = new TableConstraints();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TableConstraintsRecord> getRecordType() {
        return TableConstraintsRecord.class;
    }

    /**
     * The column <code>information_schema.table_constraints.constraint_catalog</code>.
     */
    public final TableField<TableConstraintsRecord, String> CONSTRAINT_CATALOG = createField(DSL.name("constraint_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.constraint_schema</code>.
     */
    public final TableField<TableConstraintsRecord, String> CONSTRAINT_SCHEMA = createField(DSL.name("constraint_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.constraint_name</code>.
     */
    public final TableField<TableConstraintsRecord, String> CONSTRAINT_NAME = createField(DSL.name("constraint_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.table_catalog</code>.
     */
    public final TableField<TableConstraintsRecord, String> TABLE_CATALOG = createField(DSL.name("table_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.table_schema</code>.
     */
    public final TableField<TableConstraintsRecord, String> TABLE_SCHEMA = createField(DSL.name("table_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.table_name</code>.
     */
    public final TableField<TableConstraintsRecord, String> TABLE_NAME = createField(DSL.name("table_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.constraint_type</code>.
     */
    public final TableField<TableConstraintsRecord, String> CONSTRAINT_TYPE = createField(DSL.name("constraint_type"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.is_deferrable</code>.
     */
    public final TableField<TableConstraintsRecord, String> IS_DEFERRABLE = createField(DSL.name("is_deferrable"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.table_constraints.initially_deferred</code>.
     */
    public final TableField<TableConstraintsRecord, String> INITIALLY_DEFERRED = createField(DSL.name("initially_deferred"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    private TableConstraints(Name alias, Table<TableConstraintsRecord> aliased) {
        this(alias, aliased, null);
    }

    private TableConstraints(Name alias, Table<TableConstraintsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    /**
     * Create an aliased <code>information_schema.table_constraints</code> table reference
     */
    public TableConstraints(String alias) {
        this(DSL.name(alias), TABLE_CONSTRAINTS);
    }

    /**
     * Create an aliased <code>information_schema.table_constraints</code> table reference
     */
    public TableConstraints(Name alias) {
        this(alias, TABLE_CONSTRAINTS);
    }

    /**
     * Create a <code>information_schema.table_constraints</code> table reference
     */
    public TableConstraints() {
        this(DSL.name("table_constraints"), null);
    }

    public <O extends Record> TableConstraints(Table<O> child, ForeignKey<O, TableConstraintsRecord> key) {
        super(child, key, TABLE_CONSTRAINTS);
    }

    @Override
    public Schema getSchema() {
        return InformationSchema.INFORMATION_SCHEMA;
    }

    @Override
    public TableConstraints as(String alias) {
        return new TableConstraints(DSL.name(alias), this);
    }

    @Override
    public TableConstraints as(Name alias) {
        return new TableConstraints(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TableConstraints rename(String name) {
        return new TableConstraints(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TableConstraints rename(Name name) {
        return new TableConstraints(name, null);
    }

    // -------------------------------------------------------------------------
    // Row9 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row9<String, String, String, String, String, String, String, String, String> fieldsRow() {
        return (Row9) super.fieldsRow();
    }
}
