/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.information_schema.tables;


import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.information_schema.InformationSchema;
import pt.cbr.news.generated.data.jooq.information_schema.tables.records.ColumnsRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Columns extends TableImpl<ColumnsRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>information_schema.columns</code>
     */
    public static final Columns COLUMNS = new Columns();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ColumnsRecord> getRecordType() {
        return ColumnsRecord.class;
    }

    /**
     * The column <code>information_schema.columns.table_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> TABLE_CATALOG = createField(DSL.name("table_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.table_schema</code>.
     */
    public final TableField<ColumnsRecord, String> TABLE_SCHEMA = createField(DSL.name("table_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.table_name</code>.
     */
    public final TableField<ColumnsRecord, String> TABLE_NAME = createField(DSL.name("table_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.column_name</code>.
     */
    public final TableField<ColumnsRecord, String> COLUMN_NAME = createField(DSL.name("column_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.ordinal_position</code>.
     */
    public final TableField<ColumnsRecord, Integer> ORDINAL_POSITION = createField(DSL.name("ordinal_position"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.column_default</code>.
     */
    public final TableField<ColumnsRecord, String> COLUMN_DEFAULT = createField(DSL.name("column_default"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.is_nullable</code>.
     */
    public final TableField<ColumnsRecord, String> IS_NULLABLE = createField(DSL.name("is_nullable"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.data_type</code>.
     */
    public final TableField<ColumnsRecord, String> DATA_TYPE = createField(DSL.name("data_type"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.character_maximum_length</code>.
     */
    public final TableField<ColumnsRecord, Integer> CHARACTER_MAXIMUM_LENGTH = createField(DSL.name("character_maximum_length"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.character_octet_length</code>.
     */
    public final TableField<ColumnsRecord, Integer> CHARACTER_OCTET_LENGTH = createField(DSL.name("character_octet_length"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.numeric_precision</code>.
     */
    public final TableField<ColumnsRecord, Integer> NUMERIC_PRECISION = createField(DSL.name("numeric_precision"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.numeric_precision_radix</code>.
     */
    public final TableField<ColumnsRecord, Integer> NUMERIC_PRECISION_RADIX = createField(DSL.name("numeric_precision_radix"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.numeric_scale</code>.
     */
    public final TableField<ColumnsRecord, Integer> NUMERIC_SCALE = createField(DSL.name("numeric_scale"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.datetime_precision</code>.
     */
    public final TableField<ColumnsRecord, Integer> DATETIME_PRECISION = createField(DSL.name("datetime_precision"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.interval_type</code>.
     */
    public final TableField<ColumnsRecord, String> INTERVAL_TYPE = createField(DSL.name("interval_type"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.interval_precision</code>.
     */
    public final TableField<ColumnsRecord, Integer> INTERVAL_PRECISION = createField(DSL.name("interval_precision"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.character_set_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> CHARACTER_SET_CATALOG = createField(DSL.name("character_set_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.character_set_schema</code>.
     */
    public final TableField<ColumnsRecord, String> CHARACTER_SET_SCHEMA = createField(DSL.name("character_set_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.character_set_name</code>.
     */
    public final TableField<ColumnsRecord, String> CHARACTER_SET_NAME = createField(DSL.name("character_set_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.collation_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> COLLATION_CATALOG = createField(DSL.name("collation_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.collation_schema</code>.
     */
    public final TableField<ColumnsRecord, String> COLLATION_SCHEMA = createField(DSL.name("collation_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.collation_name</code>.
     */
    public final TableField<ColumnsRecord, String> COLLATION_NAME = createField(DSL.name("collation_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.domain_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> DOMAIN_CATALOG = createField(DSL.name("domain_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.domain_schema</code>.
     */
    public final TableField<ColumnsRecord, String> DOMAIN_SCHEMA = createField(DSL.name("domain_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.domain_name</code>.
     */
    public final TableField<ColumnsRecord, String> DOMAIN_NAME = createField(DSL.name("domain_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.udt_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> UDT_CATALOG = createField(DSL.name("udt_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.udt_schema</code>.
     */
    public final TableField<ColumnsRecord, String> UDT_SCHEMA = createField(DSL.name("udt_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.udt_name</code>.
     */
    public final TableField<ColumnsRecord, String> UDT_NAME = createField(DSL.name("udt_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.scope_catalog</code>.
     */
    public final TableField<ColumnsRecord, String> SCOPE_CATALOG = createField(DSL.name("scope_catalog"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.scope_schema</code>.
     */
    public final TableField<ColumnsRecord, String> SCOPE_SCHEMA = createField(DSL.name("scope_schema"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.scope_name</code>.
     */
    public final TableField<ColumnsRecord, String> SCOPE_NAME = createField(DSL.name("scope_name"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.maximum_cardinality</code>.
     */
    public final TableField<ColumnsRecord, Integer> MAXIMUM_CARDINALITY = createField(DSL.name("maximum_cardinality"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CARDINAL_NUMBER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.dtd_identifier</code>.
     */
    public final TableField<ColumnsRecord, String> DTD_IDENTIFIER = createField(DSL.name("dtd_identifier"), pt.cbr.news.generated.data.jooq.information_schema.Domains.SQL_IDENTIFIER.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.is_self_referencing</code>.
     */
    public final TableField<ColumnsRecord, String> IS_SELF_REFERENCING = createField(DSL.name("is_self_referencing"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.is_identity</code>.
     */
    public final TableField<ColumnsRecord, String> IS_IDENTITY = createField(DSL.name("is_identity"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_generation</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_GENERATION = createField(DSL.name("identity_generation"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_start</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_START = createField(DSL.name("identity_start"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_increment</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_INCREMENT = createField(DSL.name("identity_increment"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_maximum</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_MAXIMUM = createField(DSL.name("identity_maximum"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_minimum</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_MINIMUM = createField(DSL.name("identity_minimum"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.identity_cycle</code>.
     */
    public final TableField<ColumnsRecord, String> IDENTITY_CYCLE = createField(DSL.name("identity_cycle"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.is_generated</code>.
     */
    public final TableField<ColumnsRecord, String> IS_GENERATED = createField(DSL.name("is_generated"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.generation_expression</code>.
     */
    public final TableField<ColumnsRecord, String> GENERATION_EXPRESSION = createField(DSL.name("generation_expression"), pt.cbr.news.generated.data.jooq.information_schema.Domains.CHARACTER_DATA.getDataType(), this, "");

    /**
     * The column <code>information_schema.columns.is_updatable</code>.
     */
    public final TableField<ColumnsRecord, String> IS_UPDATABLE = createField(DSL.name("is_updatable"), pt.cbr.news.generated.data.jooq.information_schema.Domains.YES_OR_NO.getDataType(), this, "");

    private Columns(Name alias, Table<ColumnsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Columns(Name alias, Table<ColumnsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    /**
     * Create an aliased <code>information_schema.columns</code> table reference
     */
    public Columns(String alias) {
        this(DSL.name(alias), COLUMNS);
    }

    /**
     * Create an aliased <code>information_schema.columns</code> table reference
     */
    public Columns(Name alias) {
        this(alias, COLUMNS);
    }

    /**
     * Create a <code>information_schema.columns</code> table reference
     */
    public Columns() {
        this(DSL.name("columns"), null);
    }

    public <O extends Record> Columns(Table<O> child, ForeignKey<O, ColumnsRecord> key) {
        super(child, key, COLUMNS);
    }

    @Override
    public Schema getSchema() {
        return InformationSchema.INFORMATION_SCHEMA;
    }

    @Override
    public Columns as(String alias) {
        return new Columns(DSL.name(alias), this);
    }

    @Override
    public Columns as(Name alias) {
        return new Columns(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Columns rename(String name) {
        return new Columns(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Columns rename(Name name) {
        return new Columns(name, null);
    }
}
