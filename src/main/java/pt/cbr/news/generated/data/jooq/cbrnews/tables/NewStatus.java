/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.cbrnews.tables;


import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row1;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import pt.cbr.news.generated.data.jooq.cbrnews.Cbrnews;
import pt.cbr.news.generated.data.jooq.cbrnews.Keys;
import pt.cbr.news.generated.data.jooq.cbrnews.tables.records.NewStatusRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class NewStatus extends TableImpl<NewStatusRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>cbrnews.new_status</code>
     */
    public static final NewStatus NEW_STATUS = new NewStatus();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<NewStatusRecord> getRecordType() {
        return NewStatusRecord.class;
    }

    /**
     * The column <code>cbrnews.new_status.name</code>.
     */
    public final TableField<NewStatusRecord, String> NAME = createField(DSL.name("name"), SQLDataType.VARCHAR(10).nullable(false), this, "");

    private NewStatus(Name alias, Table<NewStatusRecord> aliased) {
        this(alias, aliased, null);
    }

    private NewStatus(Name alias, Table<NewStatusRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>cbrnews.new_status</code> table reference
     */
    public NewStatus(String alias) {
        this(DSL.name(alias), NEW_STATUS);
    }

    /**
     * Create an aliased <code>cbrnews.new_status</code> table reference
     */
    public NewStatus(Name alias) {
        this(alias, NEW_STATUS);
    }

    /**
     * Create a <code>cbrnews.new_status</code> table reference
     */
    public NewStatus() {
        this(DSL.name("new_status"), null);
    }

    public <O extends Record> NewStatus(Table<O> child, ForeignKey<O, NewStatusRecord> key) {
        super(child, key, NEW_STATUS);
    }

    @Override
    public Schema getSchema() {
        return Cbrnews.CBRNEWS;
    }

    @Override
    public UniqueKey<NewStatusRecord> getPrimaryKey() {
        return Keys.NEW_STATUS_PKEY;
    }

    @Override
    public List<UniqueKey<NewStatusRecord>> getKeys() {
        return Arrays.<UniqueKey<NewStatusRecord>>asList(Keys.NEW_STATUS_PKEY);
    }

    @Override
    public NewStatus as(String alias) {
        return new NewStatus(DSL.name(alias), this);
    }

    @Override
    public NewStatus as(Name alias) {
        return new NewStatus(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public NewStatus rename(String name) {
        return new NewStatus(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public NewStatus rename(Name name) {
        return new NewStatus(name, null);
    }

    // -------------------------------------------------------------------------
    // Row1 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row1<String> fieldsRow() {
        return (Row1) super.fieldsRow();
    }
}
