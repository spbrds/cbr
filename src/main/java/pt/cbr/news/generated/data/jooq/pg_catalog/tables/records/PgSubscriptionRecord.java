/*
 * This file is generated by jOOQ.
 */
package pt.cbr.news.generated.data.jooq.pg_catalog.tables.records;


import org.jooq.Field;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.TableRecordImpl;

import pt.cbr.news.generated.data.jooq.pg_catalog.tables.PgSubscription;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PgSubscriptionRecord extends TableRecordImpl<PgSubscriptionRecord> implements Record6<Long, String, Long, Boolean, String, String[]> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>pg_catalog.pg_subscription.subdbid</code>.
     */
    public void setSubdbid(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subdbid</code>.
     */
    public Long getSubdbid() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>pg_catalog.pg_subscription.subname</code>.
     */
    public void setSubname(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subname</code>.
     */
    public String getSubname() {
        return (String) get(1);
    }

    /**
     * Setter for <code>pg_catalog.pg_subscription.subowner</code>.
     */
    public void setSubowner(Long value) {
        set(2, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subowner</code>.
     */
    public Long getSubowner() {
        return (Long) get(2);
    }

    /**
     * Setter for <code>pg_catalog.pg_subscription.subenabled</code>.
     */
    public void setSubenabled(Boolean value) {
        set(3, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subenabled</code>.
     */
    public Boolean getSubenabled() {
        return (Boolean) get(3);
    }

    /**
     * Setter for <code>pg_catalog.pg_subscription.subslotname</code>.
     */
    public void setSubslotname(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subslotname</code>.
     */
    public String getSubslotname() {
        return (String) get(4);
    }

    /**
     * Setter for <code>pg_catalog.pg_subscription.subpublications</code>.
     */
    public void setSubpublications(String[] value) {
        set(5, value);
    }

    /**
     * Getter for <code>pg_catalog.pg_subscription.subpublications</code>.
     */
    public String[] getSubpublications() {
        return (String[]) get(5);
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row6<Long, String, Long, Boolean, String, String[]> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    @Override
    public Row6<Long, String, Long, Boolean, String, String[]> valuesRow() {
        return (Row6) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return PgSubscription.PG_SUBSCRIPTION.SUBDBID;
    }

    @Override
    public Field<String> field2() {
        return PgSubscription.PG_SUBSCRIPTION.SUBNAME;
    }

    @Override
    public Field<Long> field3() {
        return PgSubscription.PG_SUBSCRIPTION.SUBOWNER;
    }

    @Override
    public Field<Boolean> field4() {
        return PgSubscription.PG_SUBSCRIPTION.SUBENABLED;
    }

    @Override
    public Field<String> field5() {
        return PgSubscription.PG_SUBSCRIPTION.SUBSLOTNAME;
    }

    @Override
    public Field<String[]> field6() {
        return PgSubscription.PG_SUBSCRIPTION.SUBPUBLICATIONS;
    }

    @Override
    public Long component1() {
        return getSubdbid();
    }

    @Override
    public String component2() {
        return getSubname();
    }

    @Override
    public Long component3() {
        return getSubowner();
    }

    @Override
    public Boolean component4() {
        return getSubenabled();
    }

    @Override
    public String component5() {
        return getSubslotname();
    }

    @Override
    public String[] component6() {
        return getSubpublications();
    }

    @Override
    public Long value1() {
        return getSubdbid();
    }

    @Override
    public String value2() {
        return getSubname();
    }

    @Override
    public Long value3() {
        return getSubowner();
    }

    @Override
    public Boolean value4() {
        return getSubenabled();
    }

    @Override
    public String value5() {
        return getSubslotname();
    }

    @Override
    public String[] value6() {
        return getSubpublications();
    }

    @Override
    public PgSubscriptionRecord value1(Long value) {
        setSubdbid(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord value2(String value) {
        setSubname(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord value3(Long value) {
        setSubowner(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord value4(Boolean value) {
        setSubenabled(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord value5(String value) {
        setSubslotname(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord value6(String[] value) {
        setSubpublications(value);
        return this;
    }

    @Override
    public PgSubscriptionRecord values(Long value1, String value2, Long value3, Boolean value4, String value5, String[] value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PgSubscriptionRecord
     */
    public PgSubscriptionRecord() {
        super(PgSubscription.PG_SUBSCRIPTION);
    }

    /**
     * Create a detached, initialised PgSubscriptionRecord
     */
    public PgSubscriptionRecord(Long subdbid, String subname, Long subowner, Boolean subenabled, String subslotname, String[] subpublications) {
        super(PgSubscription.PG_SUBSCRIPTION);

        setSubdbid(subdbid);
        setSubname(subname);
        setSubowner(subowner);
        setSubenabled(subenabled);
        setSubslotname(subslotname);
        setSubpublications(subpublications);
    }
}
